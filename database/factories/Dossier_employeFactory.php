<?php

namespace Database\Factories;

use App\Models\Dossier_employe;
use Illuminate\Database\Eloquent\Factories\Factory;

class Dossier_employeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Dossier_employe::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id_employe' => $this->faker->randomDigitNotNull,
        'cv' => $this->faker->word,
        'lettre_motivation' => $this->faker->word,
        'type_piece' => $this->faker->word,
        'number_of_piece' => $this->faker->word,
        'diplome' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
