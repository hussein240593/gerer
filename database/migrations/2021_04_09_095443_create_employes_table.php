<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employes', function (Blueprint $table) {
            $table->id();
            $table->foreign("departement_id")->references("id")
                ->on("departements")->onDelete("cascade")->onUpdate('cascade');
            $table->integer('departement_id')->nullable();

            $table->integer('fonction_id')->nullable();
            $table->foreign("fonction_id")->references("id")
                ->on("fonctions")->onDelete("cascade")->onUpdate('cascade');

            $table->string('nom')->nullable();
            $table->string('prenom')->nullable();
            $table->string('sexe')->nullable();
            $table->string('adr_postal')->nullable();
            $table->string('email')->nullable()->unique();
            $table->integer('ville_id')->nullable();
            $table->foreign("ville_id")->references("id")
            ->on("ville")->onDelete("cascade")->onUpdate('cascade');

            $table->string('telephone')->nullable();
            $table->string('birthday')->nullable();

            $table->integer('type_fonction_id')->nullable();
            $table->foreign("type_fonction_id")->references("id")
            ->on("type_fonctions")->onDelete("cascade")->onUpdate('cascade');

            // $table->string('id_fonction');
            $table->string('slug')->nullable();
            // demission
            $table->string('motif')->nullable();
            $table->string('date_demission')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employes');
    }
}
