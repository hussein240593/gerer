<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formations', function (Blueprint $table) {
            $table->id();
            $table->string('libelle')->nullable();
            $table->string('slug')->nullable();
            $table->integer('type_formation_id')->nullable();
            $table->string('date_debut')->nullable();
            $table->date('date_fin')->nullable();
            $table->integer('employe_id')->nullable();
            $table->foreign("employe_id")->references("id")
            ->on("employes")->onDelete("cascade")->onUpdate('cascade');
            $table->string('number_of_day')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formations');
    }
}
