<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSanctionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sanctions', function (Blueprint $table) {

            $table->id();
            $table->integer('employe_id')->nullable();
            $table->foreign("employe_id")->references("id")
            ->on("employes")->onDelete("cascade")->onUpdate('cascade');
            $table->string('Motif')->nullable();
            $table->string('Date_de_comise_la_faute')->nullable();
            $table->string('type_sanction_id')->nullable()->comment('renvoi,licencement');
            $table->foreign("type_sanction_id")->references("id")
            ->on("type_sanctions")->onDelete("cascade")->onUpdate('cascade');
            $table->string('date_debut')->nullable();
            $table->string('date_fin')->nullable();
            $table->string('slug')->nullable();
            // Decision de sanction
            $table->string('decisionSanction')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sanctions');
    }
}
