<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDossierEmployesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dossier_employes', function (Blueprint $table) {
            $table->id();
            $table->foreign("employe_id")->references("id")
                ->on("employes")->onDelete("cascade");
            $table->integer('employe_id')->nullable();
            $table->string('cv')->nullable();
            $table->string('lettre_motivation')->nullable();
            $table->string('type_piece')->nullable();
            $table->string('number_of_piece')->nullable();
            $table->string('diplome')->nullable();
            $table->string('slug')->nullable();
            $table->string('avatar')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dossier_employes');
    }
}
