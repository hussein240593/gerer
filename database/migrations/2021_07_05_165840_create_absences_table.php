<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAbsencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('absences', function (Blueprint $table) {
            $table->id();
            $table->integer('employe_id')->nullable();
            $table->foreign("employe_id")->references("id")
            ->on("employes")->onDelete("cascade")->onUpdate('cascade');
            // $table->string('date_debut')->nullable();
            // $table->string('date_fin')->nullable();
            $table->string('date_absence')->nullable();
            $table->string('slug')->nullable();
            // Justification d'absence
            $table->string('rapport_explication')->default(0);
            // Status pour le pointage,demision,absence et presence
            $table->string('is_status_present')->default(0);
            // Accepter ou refus de la demission
            $table->string('is_etat_accepte_demission')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('absences');
    }
}
