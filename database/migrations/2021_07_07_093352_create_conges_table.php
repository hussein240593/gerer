<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCongesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conges', function (Blueprint $table) {
            $table->id();
            $table->integer('employe_id')->nullable();
            $table->foreign("employe_id")->references("id")
            ->on("employes")->onDelete("cascade")->onUpdate('cascade');
            // $table->string('nombre_jours_conge')->nullable();
            $table->string('date_debut_conge')->nullable();
            $table->date('date_fin')->nullable();
            $table->text('motif_conge')->nullable();
            $table->string('slug')->nullable();
            $table->string('is_motif_refus')->default(0);
            $table->string('lettre_autorisation')->default(NULL);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conges');
    }
}
