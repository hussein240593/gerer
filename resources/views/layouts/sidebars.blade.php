<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('/')}}" class="brand-link">
      <img src="dist/img/AdminLTELogo.pn" alt="" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Gérer</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="pb-3 mt-3 mb-3 user-panel d-flex">
        <div class="image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->name ?? 'Hors ligne'}}</a>
        </div>
      </div>
      <ul class="nav nav-pills nav-sidebar flex-column">

        <li class="nav-header" style="background: white;color:black; border-radius: 17px;">
            <a href="{{url('home')}}"style="color:black">
              <i class="fas fa-cog nav-icon"></i>
              Administration
          </a>

      </li>
      </ul>

      <!-- SidebarSearch Form -->
      {{-- <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div> --}}

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

          <li class="nav-header">Gestion des employés</li>
          <li class="nav-item">
            <a href="{{route('employes.index')}}" class="nav-link">
              <i class="nav-icon far fa-user"></i>
              <p>
                Employés
                {{-- <span class="badge badge-info right">2</span> --}}
              </p>
            </a>
          </li>
          {{-- <li class="nav-item">
            <a href="pages/gallery.html" class="nav-link">
              <i class="nav-icon far fa-image"></i>
              <p>
                Gallery
              </p>
            </a>
          </li> --}}


          {{-- <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Pages
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/examples/invoice.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Invoice</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/examples/profile.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Profile</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/examples/e-commerce.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>E-commerce</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/examples/projects.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Projects</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/examples/project-add.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Project Add</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/examples/project-edit.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Project Edit</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/examples/project-detail.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Project Detail</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/examples/contacts.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Contacts</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/examples/faq.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>FAQ</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/examples/contact-us.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Contact us</p>
                </a>
              </li>
            </ul>
          </li> --}}


          <li class="nav-header">Gestion des sanctions</li>
          <li class="nav-item">
            <a href="{{route('sanctions.index')}}" class="nav-link">
              <i class="nav-icon fas fa-tag"></i>
              <p>Sanctions</p>
            </a>
          </li>
          <li class="nav-header">Gestion des pointages</li>
          <li class="nav-item">
            <a href="{{route('conges.index')}}" class="nav-link">
              <i class="nav-icon fas fa-indent"></i>
              <p>Congés</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{route('absences.index')}}"  class="nav-link">
              <i class="nav-icon fas fa-tablet"></i>
              <p>Absences</p>
            </a>
          </li>
          <li class="nav-header">Gestion des formations</li>
          <li class="nav-item">
            <a href="{{route('formations.index')}}" class="nav-link">
              <i class="fas fa-circle nav-icon"></i>
              <p>Formations</p>
            </a>
          </li>
          <li class="nav-header">Gestion des activités récreatives</li>

          {{-- <li class="nav-header">Compte</li> --}}


        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

