  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <h5 class="mb-2">MENU</h5>
      <div class="row">
        <div class="col-md-3 col-sm-6 col-12">
          <div class="info-box">
            <span class="info-box-icon bg-info"><i class="far fa-user"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"><a href="{{route('employes.index')}}">Gestion des employé</a></span>
              <span class="info-box-number">1,410</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-12">
          <div class="info-box">
            <span class="info-box-icon bg-success"><i class="fas fa-tag"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"><a href="{{route('formations.index')}}">Gestion des formations</a></span>
              <span class="info-box-number">410</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-12">
          <div class="info-box">
            <span class="info-box-icon bg-warning"><i class="fas fa-exclamation"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"><a href="{{route('sanctions.index')}}">Gestion des sanctions</a></span>
              <span class="info-box-number">13,648</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-12">
          <div class="info-box">
            <span class="info-box-icon bg-danger"><i class="fas fa-indent"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"><a href="{{route('absences.index')}}">Gestion des pointages</a></span>
              <span class="info-box-number">93,139</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- =========================================================== -->

      <div class="row">
        <div class="col-md-3 col-sm-6 col-12">
          <div class="shadow-none info-box">
            <span class="info-box-icon bg-info"><i class="fas fa-tablet"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"><a href="{{route('conges.index')}}">Gestion des congés</a></span>
              <span class="info-box-number">122</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-12">
          <div class="shadow-sm info-box">
            <span class="info-box-icon bg-success"><i class="far fa-flag"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"><a href="#">Activités recreatives</a></span>
              <span class="info-box-number">Small</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-12">
          <div class="shadow info-box">
            <span class="info-box-icon bg-warning"><i class="far fa-copy"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Shadows</span>
              <span class="info-box-number">Regular</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-12">
          <div class="shadow-lg info-box">
            <span class="info-box-icon bg-danger"><i class="far fa-star"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Shadows</span>
              <span class="info-box-number">Large</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->


    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
