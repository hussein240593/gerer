

<li class="nav-item">
    <a href="{{ route('typeSanctions.index') }}"
       class="nav-link {{ Request::is('typeSanctions*') ? 'active' : '' }}">
        <p>Type Sanctions</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('groupes.index') }}"
       class="nav-link {{ Request::is('groupes*') ? 'active' : '' }}">
        <p>Groupes</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('dossierEmployes.index') }}"
       class="nav-link {{ Request::is('dossierEmployes*') ? 'active' : '' }}">
        <p>Dossier Employes</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('departements.index') }}"
       class="nav-link {{ Request::is('departements*') ? 'active' : '' }}">
        <p>Departements</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('fonctions.index') }}"
       class="nav-link {{ Request::is('fonctions*') ? 'active' : '' }}">
        <p>Fonctions</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('typeFonctions.index') }}"
       class="nav-link {{ Request::is('typeFonctions*') ? 'active' : '' }}">
        <p>Type Fonctions</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('typeFormations.index') }}"
       class="nav-link {{ Request::is('typeFormations*') ? 'active' : '' }}">
        <p>Type Formations</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('villes.index') }}"
       class="nav-link {{ Request::is('villes*') ? 'active' : '' }}">
        <p>Villes</p>
    </a>
</li>


