<div class="table-responsive">
    <table class="table" id="typeSanctions-table">
        <thead>
        <tr>
            <th>Libelle</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($typeSanctions as $typeSanction)
            <tr>
                <td>{{ $typeSanction->libelle }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['typeSanctions.destroy', $typeSanction->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('typeSanctions.show', [$typeSanction->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('typeSanctions.edit', [$typeSanction->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
