<div class="table-responsive">
    <table class="table" id="dossierEmployes-table">
        <thead>
        <tr>
            <th>Id Employe</th>
        <th>Cv</th>
        <th>Lettre Motivation</th>
        <th>Type Piece</th>
        <th>Number Of Piece</th>
        <th>Diplome</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($dossierEmployes as $dossierEmploye)
            <tr>
                <td>{{ $dossierEmploye->id_employe }}</td>
            <td>{{ $dossierEmploye->cv }}</td>
            <td>{{ $dossierEmploye->lettre_motivation }}</td>
            <td>{{ $dossierEmploye->type_piece }}</td>
            <td>{{ $dossierEmploye->number_of_piece }}</td>
            <td>{{ $dossierEmploye->diplome }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['dossierEmployes.destroy', $dossierEmploye->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('dossierEmployes.show', [$dossierEmploye->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('dossierEmployes.edit', [$dossierEmploye->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
