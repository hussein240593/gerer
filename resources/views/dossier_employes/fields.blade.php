<!-- Id Employe Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_employe', 'Id Employe:') !!}
    {!! Form::select('id_employe', ], null, ['class' => 'form-control custom-select']) !!}
</div>


<!-- Cv Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cv', 'Cv:') !!}
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('cv', ['class' => 'custom-file-input']) !!}
            {!! Form::label('cv', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
</div>
<div class="clearfix"></div>


<!-- Lettre Motivation Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lettre_motivation', 'Lettre Motivation:') !!}
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('lettre_motivation', ['class' => 'custom-file-input']) !!}
            {!! Form::label('lettre_motivation', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
</div>
<div class="clearfix"></div>


<!-- Type Piece Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type_piece', 'Type Piece:') !!}
    {!! Form::select('type_piece', ], null, ['class' => 'form-control custom-select']) !!}
</div>


<!-- Number Of Piece Field -->
<div class="form-group col-sm-6">
    {!! Form::label('number_of_piece', 'Number Of Piece:') !!}
    {!! Form::text('number_of_piece', null, ['class' => 'form-control']) !!}
</div>

<!-- Diplome Field -->
<div class="form-group col-sm-6">
    {!! Form::label('diplome', 'Diplome:') !!}
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('diplome', ['class' => 'custom-file-input']) !!}
            {!! Form::label('diplome', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
</div>
<div class="clearfix"></div>
