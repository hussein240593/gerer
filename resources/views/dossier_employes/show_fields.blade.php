<!-- Id Employe Field -->
<div class="col-sm-12">
    {!! Form::label('id_employe', 'Id Employe:') !!}
    <p>{{ $dossierEmploye->id_employe }}</p>
</div>

<!-- Cv Field -->
<div class="col-sm-12">
    {!! Form::label('cv', 'Cv:') !!}
    <p>{{ $dossierEmploye->cv }}</p>
</div>

<!-- Lettre Motivation Field -->
<div class="col-sm-12">
    {!! Form::label('lettre_motivation', 'Lettre Motivation:') !!}
    <p>{{ $dossierEmploye->lettre_motivation }}</p>
</div>

<!-- Type Piece Field -->
<div class="col-sm-12">
    {!! Form::label('type_piece', 'Type Piece:') !!}
    <p>{{ $dossierEmploye->type_piece }}</p>
</div>

<!-- Number Of Piece Field -->
<div class="col-sm-12">
    {!! Form::label('number_of_piece', 'Number Of Piece:') !!}
    <p>{{ $dossierEmploye->number_of_piece }}</p>
</div>

<!-- Diplome Field -->
<div class="col-sm-12">
    {!! Form::label('diplome', 'Diplome:') !!}
    <p>{{ $dossierEmploye->diplome }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $dossierEmploye->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $dossierEmploye->updated_at }}</p>
</div>

