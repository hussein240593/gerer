<div class="table-responsive">
    <table class="table" id="villes-table">
        <thead>
        <tr>
            <th>Libelle</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($villes as $ville)
            <tr>
                <td>{{ $ville->libelle }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['villes.destroy', $ville->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('villes.show', [$ville->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('villes.edit', [$ville->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
