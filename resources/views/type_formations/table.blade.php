<div class="table-responsive">
    <table class="table" id="typeFormations-table">
        <thead>
        <tr>
            <th>Libelle</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($typeFormations as $typeFormation)
            <tr>
                <td>{{ $typeFormation->libelle }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['typeFormations.destroy', $typeFormation->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('typeFormations.show', [$typeFormation->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('typeFormations.edit', [$typeFormation->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
