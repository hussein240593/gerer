@extends('layouts.main')
@section('content')


  <!-- Navbar -->
    @include('layouts.header')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->

  @include('layouts.sidebars')
  <!-- Content Wrapper. Contains page content -->
 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="mb-2 row">
          <div class="col-sm-6">
            {{-- <h1 class="m-0">Tableau de bord</h1> --}}
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">créer un departement</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
        <div class="row">
            <div class="col-12">


            <div class="card">
                <div class="card-header">
                <h3 class="card-title">Créer un departement</h3>
                <span class="breadcrumb float-sm-right"><a href="{{route('departements.index')}}">Retour</a></span>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                     <!-- form start -->
              <form method="POST" enctype="multipart/form-data" action="{{route('departements.store')}}">
                @csrf
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Libelle</label>
                                <input type="text"name="nom_depart" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                              </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Ville departement</label>
                                <input type="text" name="ville_depart" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                              </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Téléphone departement</label>
                                <input type="text" name="tel" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                              </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Adresse rue</label>
                                <input type="text"name="adr_rue" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                              </div>
                        </div>
                        
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Adresse ville</label>
                                <input type="text"name="adr_ville_depart" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                              </div>
                        </div>
                        
                    </div>

                <div class="card-footer">
                  <button type="submit" class="btn btn-success">Enregistrer</button>
                </div>
              </form>

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  {{-- footer --}}
  @include('layouts.footer')
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

@endsection
