@extends('main')

@section('content')



 <!-- Wrapper Start -->
 <div class="wrapper"
 style="background: url(../assets/images/background.png); background-attachment: fixed; background-size: cover; ">
	<!-- Main sidebar -->
    @include('layouts.sidebars')
    <!-- /main sidebar -->
	<!-- Main navbar -->
    @include('layouts.header')
	<!-- /main navbar -->

    <div class="content-page">

        <div class="container-fluid">
            <div class="row">
               <div class="col-lg-12">
                  <div class="card car-transparent">
                     <div class="p-0 card-body">
                        <div class="profile-image position-relative">
                           <img src="{{asset('assets/images/page-img/profile.png')}}" class="rounded img-fluid w-100" alt="profile-image">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="px-3 row m-sm-0">
               <div class="col-lg-6 card-profile col-md-6 col-xl-4">
                  <div class="card card-block card-stretch card-height">
                     <div class="card-body">
                        <div class="mb-3 d-flex align-items-center">
                           <div class="profile-img position-relative">
                              <img src="../assets/images/user/1.jpg" class="rounded img-fluid avatar-110" alt="profile-image">
                           </div>
                           <div class="ml-3">
                              <h4 class="mb-1">{{$employe->nom}} {{$employe->prenom}}</h4>
                              <p class="mb-2">
                                @php
                                    $typeFonction = App\Models\Type_fonction::where('id',$employe->id_type_fonction)->find($employe->id_type_fonction);
                                    if($typeFonction===null){
                                        echo 'Le type fonction a été supprimé';
                                    }else{
                                        echo $typeFonction->libelle;
                                    }
                                @endphp
                              </p>
                              <a href="#" class="btn btn-primary font-size-14">Contact</a>
                           </div>
                        </div>
                        <p>
                           I’m a Ux/UI designer. I spend my whole day,

                        </p>
                        <ul class="p-0 m-0 list-inline">



                           <li class="mb-2">
                              <div class="d-flex align-items-center">
                                 <svg class="mr-3 svg-icon" height="16" width="16" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 5a2 2 0 012-2h3.28a1 1 0 01.948.684l1.498 4.493a1 1 0 01-.502 1.21l-2.257 1.13a11.042 11.042 0 005.516 5.516l1.13-2.257a1 1 0 011.21-.502l4.493 1.498a1 1 0 01.684.949V19a2 2 0 01-2 2h-1C9.716 21 3 14.284 3 6V5z" />
                                 </svg>
                                 <p class="mb-0">{{$employe->telephone}}</p>
                              </div>
                           </li>
                           <li>
                              <div class="d-flex align-items-center">
                                 <svg class="mr-3 svg-icon" height="16" width="16" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 8l7.89 5.26a2 2 0 002.22 0L21 8M5 19h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" />
                                 </svg>
                                 <p class="mb-0"><a href="https://templates.iqonic.design/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="773d18161933021837070518071205030e5914181a">{{$employe->email}}</a></p>
                              </div>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="col-lg-6 card-profiles col-md-6 col-xl-8">
                  <div class="card card-block card-stretch card-height">
                     <div class="card-body">
                        <ul class="mb-3 text-center d-flex nav nav-pills profile-tab" id="profile-pills-tab" role="tablist">

                           <li class="nav-item">
                               <a class="nav-link active show" data-toggle="pill" href="#profile3" role="tab" aria-selected="false">Information personnelle</a>
                           </li>
                           <li class="nav-item">
                               <a class="nav-link" data-toggle="pill" href="#profile4" role="tab" aria-selected="false">Poste</a>
                           </li>

                       </ul>
                        <div class="profile-content tab-content">



                           <div id="profile3" class="tab-pane active show fade">
                              <div class="m-0 profile-line d-flex align-items-center justify-content-between position-relative">
                                 <ul class="p-0 m-0 list-inline w-100">
                                    <li>
                                       <div class="row align-items-top">
                                          <div class="col-md-3">
                                             <h6 class="mb-2">Nom</h6>
                                          </div>
                                          <div class="col-md-9">
                                             <div class="media profile-media align-items-top">
                                                {{-- <div class="mt-1 profile-dots border-primary"></div> --}}
                                                <div class="ml-4">
                                                   <h6 class="mb-1 ">{{$employe->nom}}</h6>
                                                   {{-- <p class="mb-0 font-size-14">South Kellergrove University</p> --}}
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="row align-items-top">
                                          <div class="col-md-3">
                                             <h6 class="mb-2">Prenom</h6>
                                          </div>
                                          <div class="col-md-9">
                                             <div class="media profile-media align-items-top">
                                                {{-- <div class="mt-1 profile-dots border-primary"></div> --}}
                                                <div class="ml-4">
                                                   <h6 class="mb-1 ">{{$employe->prenom}}</h6>
                                                   {{-- <p class="mb-0 font-size-14">Milchuer College</p> --}}
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="row align-items-top">
                                          <div class="col-md-3">
                                          <h6 class="mb-2">Ville</h6>
                                          </div>
                                          <div class="col-md-9">
                                             <div class="media profile-media align-items-top">
                                                {{-- <div class="mt-1 profile-dots border-primary"></div> --}}
                                                <div class="ml-4">
                                                   <h6 class="mb-1 ">{{$employe->ville}}</h6>
                                                   {{-- <p class="mb-0 font-size-14">Beechtown Universityy</p> --}}
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </li>

                                 </ul>
                              </div>
                           </div>
                           <div id="profile4" class="tab-pane fade">
                              <div class="m-0 profile-line d-flex align-items-center justify-content-between position-relative">
                                 <ul class="p-0 m-0 list-inline w-100">
                                    <li>
                                       <div class="row align-items-top">
                                          <div class="col-md-3">
                                             <h6 class="mb-2">Salaire</h6>
                                          </div>
                                          <div class="col-md-9">
                                             <div class="media profile-media align-items-top">
                                                {{-- <div class="mt-1 profile-dots border-primary"></div> --}}
                                                <div class="ml-4">
                                                   <h6 class="mb-1 ">{{$employe->salaire ?? ''}}</h6>
                                                   {{-- <p class="mb-0 font-size-14">Total : 02 + years of experience</p> --}}
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="row align-items-top">
                                          <div class="col-md-3">
                                             <h6 class="mb-2">Fonction</h6>
                                          </div>
                                          <div class="col-md-9">
                                             <div class="media profile-media align-items-top">
                                                {{-- <div class="mt-1 profile-dots border-primary"></div> --}}
                                                <div class="ml-4">
                                                   <h6 class="mb-1 ">
                                                    @php
                                                        $fonction = App\Models\Fonction::where('id',$employe->id_fonction)->find($employe->id_fonction);
                                                        if($fonction===null){
                                                            echo 'La fonction a été supprimé';
                                                        }else{
                                                            echo $fonction->libelle;
                                                        }
                                                    @endphp
                                                   </h6>
                                                   {{-- <p class="mb-0 font-size-14">Total : 1.5 + years of experience</p> --}}
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="row align-items-top">
                                          <div class="col-md-3">
                                          <h6 class="mb-2">Departement</h6>
                                          </div>
                                          <div class="col-md-9">
                                             <div class="media profile-media align-items-top">
                                                {{-- <div class="mt-1 profile-dots border-primary"></div> --}}
                                                <div class="ml-4">
                                                   <h6 class="mb-1 ">
                                                    @php
                                                        $departement = App\Models\Departement::where('id',$employe->departement_id)->find($employe->departement_id);
                                                        if($departement===null){
                                                            echo 'Le departement a été supprimé';
                                                        }else{
                                                            echo $departement->nom_depart;
                                                        }
                                                    @endphp
                                                   </h6>
                                                   {{-- <p class="mb-0 font-size-14">Total : 0.5 + years of experience</p> --}}
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="row align-items-top">
                                          <div class="col-3">
                                             <h6 class="mb-2">Type fonction</h6>
                                          </div>
                                          <div class="col-9">
                                             <div class="pb-0 media profile-media align-items-top">
                                                {{-- <div class="mt-1 profile-dots border-primary"></div> --}}
                                                <div class="ml-4">
                                                   <h6 class="mb-1 ">
                                                    @php
                                                        $typeFonction = App\Models\Type_fonction::where('id',$employe->id_type_fonction)->find($employe->id_type_fonction);
                                                        if($typeFonction===null){
                                                            echo 'Le type fonction a été supprimé';
                                                        }else{
                                                            echo $typeFonction->libelle;
                                                        }
                                                    @endphp
                                                   </h6>
                                                   {{-- <p class="mb-0 font-size-14">Total : Freshers</p> --}}
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </li>
                                 </ul>
                              </div>
                           </div>

                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>

    </div>
      <!-- Page end  -->
 @include('layouts.footer')
</div>
<!-- Wrapper End-->
	<!-- /page content -->
@endsection
