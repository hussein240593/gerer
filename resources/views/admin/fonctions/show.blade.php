@extends('main')

@section('content')



 <!-- Wrapper Start -->
 <div class="wrapper"
 style="background: url(../assets/images/background.png); background-attachment: fixed; background-size: cover; ">
	<!-- Main sidebar -->
    @include('layouts.sidebars')
    <!-- /main sidebar -->
	<!-- Main navbar -->
    @include('layouts.header')
	<!-- /main navbar -->

    <div class="content-page">

        <div class="card">
            <div class="card-header d-flex justify-content-between">
               <div class="header-title">
                  <h4 class="card-title">List Active</h4>
               </div>
            </div>
            <div class="card-body">
               <p>Add <code>.active</code> to a <code>.list-group-item</code> to indicate the current active selection.</p>
               <ul class="list-group">
                  <li class="list-group-item active">Cras justo odio</li>
                  <li class="list-group-item">Dapibus ac facilisis in</li>
                  <li class="list-group-item">Morbi leo risus</li>
                  <li class="list-group-item">Porta ac consectetur ac</li>
                  <li class="list-group-item">Vestibulum at eros</li>
               </ul>
            </div>
         </div>

    </div>
      <!-- Page end  -->
 @include('layouts.footer')
</div>
<!-- Wrapper End-->
	<!-- /page content -->
@endsection
