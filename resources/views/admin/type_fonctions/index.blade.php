@extends('layouts.main')
@section('content')


  <!-- Navbar -->
    @include('layouts.header')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->

  @include('layouts.sidebars')
  <!-- Content Wrapper. Contains page content -->
 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="mb-2 row">
          <div class="col-sm-6">
            <h1 class="m-0">Tableau de bord</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Employés</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    
 <!-- Main content -->
 <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">


          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Liste des types de fonctions</h3>
              <span class="breadcrumb float-sm-right"><a href="{{route('typeFonctions.create')}}">Ajouter</a></span>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Libelle</th>

                    <th class="min-width: 100px">Actions</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($type_fonctions as $type_fonction)
                    <tr>
                      


                       <td>{{$type_fonction->libelle}}</td>



                       <td>
                           <div class="d-flex align-items-center list-user-action">

                              <div class="d-flex align-items-center list-user-action">
                            <div class="card-body">
                                <button type="button" class="mr-1 btn btn-sm bg-primary" data-toggle="modal" data-target="#modal-justify{{$type_fonction->id}}">
                                    <i class="far fa-eye"></i>
                                </button>
                            </div>
                              
                            <div class="modal fade" id="modal-justify{{$type_fonction->id}}">
                                <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <h4 class="modal-title">Details</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>
                                    <div class="modal-body">
                                   <!-- Table row -->
                                   <div class="row">
                                    <div class="col-12 table-responsive">
                                      <table class="table table-striped">
                                          <thead>
                                          <tr>
                                            <th>Libelle</th>

                                            <th class="min-width: 100px">Actions</th>
                                    
                                          </tr>
                                          </thead>
                                          <tbody>
                                          <tr>
                                            <td>{{$type_fonction->libelle}}</td>

                                          </tr>
                                          </tbody>
                                        </table>
                                    </div>
                                    <!-- /.col -->
                                  </div>
                                  <!-- /.row -->
                                    </div>
                                    <div class="modal-footer justify-content-between">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                              <a class="mr-1 btn btn-sm bg-primary" data-toggle="tooltip" data-placement="top" title=""
                                 data-original-title="Edit" href="{{route('typeFonctions.edit',$type_fonction->id)}}"><i class="far fa-edit"></i></a>

                               <form class="btn btn-sm bg-danger" data-toggle="tooltip" data-placement="top" title=""
                               data-original-title="Delete" method="POST" action="{{route('typeFonctions.destroy',$type_fonction->id)}}">
                                   @csrf
                                   @method('DELETE')
                                   <button type="submit" class=""><i class="far fa-trash-alt"></i></button>

                               </form>
                           </div>
                        </td>
                    </tr>
                   @endforeach
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  {{-- footer --}}
  @include('layouts.footer')
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

@endsection
