@extends('layouts.main')
@section('content')


  <!-- Navbar -->
    @include('layouts.header')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->

  @include('layouts.sidebars')
  <!-- Content Wrapper. Contains page content -->
 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="mb-2 row">
          <div class="col-sm-6">
            {{-- <h1 class="m-0">Tableau de bord</h1> --}}
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Accueil</a></li>
              <li class="breadcrumb-item active">Type formation</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    
 <!-- Main content -->
 <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">


          <div class="card card-info">
            <div class="card-header">
              <h3 class="card-title">Liste type formation</h3>
              <span class="breadcrumb float-sm-right"><a href="{{route('typeFormations.create')}}">Ajouter</a></span>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Libelle</th>
                    <th>Description</th>
                    <th class="min-width: 100px">Actions</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($types_formations as $type_formation)
                        <tr>
                            <td>{{$type_formation->libelle}}</td>
                            <td>{{$type_formation->description}}</td>
                            <td>
                                <div class="d-flex align-items-center list-user-action">
                                    <a class="mr-1 btn btn-sm bg-primary" data-toggle="tooltip" data-placement="top" title=""
                                        data-original-title="Voir profil" href="{{route('typeFormations.show',$type_formation->id)}}">
                                        <i class="far fa-eye"></i>
                                    </a>
                                    <a class="mr-1 btn btn-sm bg-primary" data-toggle="tooltip" data-placement="top" title=""
                                        data-original-title="Edit" href="{{route('typeFormations.edit',$type_formation->id)}}"><i class="far fa-edit"></i></a>

                                    <form class="btn btn-sm bg-danger" data-toggle="tooltip" data-placement="top" title=""
                                    data-original-title="Delete" method="POST" action="{{route('typeFormations.destroy',$type_formation->id)}}">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class=""><i class="far fa-trash-alt"></i></button>

                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  {{-- footer --}}
  @include('layouts.footer')
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

@endsection
