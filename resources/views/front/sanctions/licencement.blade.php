<div class="card-body">
    <table id="example1" class="table table-bordered table-striped">
      <thead>
          <tr class="ligth">
              <th>Nom et prenom</th>
              <th>date absence</th>
              {{-- <th>Date fin</th> --}}

              <th>Rapport de demission</th>
              <th>Status</th>
              <th>Etat</th>

              {{-- <th class="min-width: 100px">Actions</th> --}}

           </tr>
      </thead>
      <tbody>
          @foreach($absences as $absence)
            @if ($absence->is_status_present==5)


              <tr>

                  <td>
                      @php
                          $employes = App\Models\Employe::where('id',$absence->employe_id)->find($absence->employe_id);
                          if($employes===null){
                              echo 'Le nom a été supprimé';
                          }else{
                              echo "$employes->nom $employes->prenom";
                          }
                      @endphp
                  </td>
                  <td>{{ \Carbon\Carbon::parse(($absence->date_absence))->isoFormat('LLL') }}</td>
                  {{-- <td>{{date('d/m/Y',strtotime($absence->date_absence))}}</td> --}}


                  <td>

                      {{-- <a href="" class="badge badge-danger" data-toggle="tooltip" data-placement="top" title=""
                          data-original-title="Non justifié">
                          Non justifié
                      </a> --}}
                      @if($absence->rapport_explication=="0")
                      {{-- <div class="card-body"> --}}
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-lg2{{$absence->id}}">
                            Rediger un rapport
                        </button>
                      {{-- </div> --}}
                      @else
                      <div class="card-body">

                        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-lg{{$absence->id}}">
                            Voir le rapport
                          </button>
                        </div>
                      @endif

                         {{-- MODAL1 Absence non justifié --}}
                      <div class="modal fade" id="modal-lg2{{$absence->id}}">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h4 class="modal-title">
                                @php
                                    $employes = App\Models\Employe::where('id',$absence->employe_id)->find($absence->employe_id);
                                    if($employes===null){
                                        echo 'Le nom a été supprimé';
                                    }else{
                                        echo "$employes->nom $employes->prenom "."veuillez vous justifier";
                                    }
                                @endphp
                              </h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <p>
                                <form method="POST" enctype="multipart/form-data" class="wizard-form" action="{{route('justify.index',$absence->id)}}">
                                    @csrf
                                    {{-- @method('put') --}}
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Rediger votre rapport</label>
                                                <textarea required type="text" name="rapport_explication" class="form-control" id="mobno" placeholder="Remplissez cette fiche de demande d'explication pour justifier votre absence du {{$absence->date_absence}} "></textarea>
                                            </div>
                                        </div>

                                        {{-- <div class="col-sm-2">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Selectionner</label>
                                                <input type="file" name="rapport_explication" class="form-control" id="mobno" placeholder="Remplissez cette fiche de demande d'explication pour justifier votre absence du {{$absence->date_absence}} ">
                                            </div>
                                        </div> --}}
                                    </div>

                                    <div class="modal-footer justify-content-between">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">fermer</button>
                                        <button type="submit" class="btn btn-primary">Justifier</button>
                                      </div>
                                </form>
                              </p>
                            </div>

                          </div>
                          <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                      </div>
                      <!-- /.modal -->


                      {{-- MODAL2 Absence justifié --}}
                      <div class="modal fade" id="modal-lg{{$absence->id}}">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="ribbon-wrapper ribbon-lg">
                                <div class="ribbon bg-info">
                                      @php
                                          $employes = App\Models\Employe::where('id',$absence->employe_id)->find($absence->employe_id);
                                          if($employes===null){
                                              echo 'Le nom a été supprimé';
                                          }else{
                                              echo "$employes->nom $employes->prenom ";
                                          }
                                      @endphp
                                </div>
                              </div>
                            <div class="modal-header">
                              <h4 class="modal-title">Rapport de demission</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <p>{{$absence->rapport_explication}}&hellip;</p>
                            </div>
                            <div class="modal-footer justify-content-between">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
                            </div>
                          </div>
                          <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                      </div>
                      <!-- /.modal -->


                    </td>
                    <td>
                        @if($absence->is_etat_accepte_demission==0)
                        <a class="btn btn-success"href="#">
                            Nouvelle demande
                        </a>
                        @elseif($absence->is_etat_accepte_demission==1)
                        <a class="btn btn-default" href="#">
                            Cet employé à demissionné
                           {{-- Cet employé a rendu sa demission --}}
                        </a>
                        @else
                        <a class="btn btn-info" href="#">
                           Demande refusé
                         </a>
                        @endif

                    </td>
                    <td>
                        @if($absence->is_etat_accepte_demission==1)
                        <a class="btn-iframe-close" style="color:#dc3545" data-widget="iframe-close"data-type="only-this" href="#">
                           <i class="fas fa-times"></i>
                        </a>


                        @elseif($absence->is_etat_accepte_demission==2)
                        <a class="" href="#">
                           <i class="nav-icon far fa-circle text-info"></i>

                         </a>
                         @else
                         <a class="btn btn-outline-success" href="{{route('accepte_demission',$absence->id)}}">
                            Accepté
                        </a>
                        <a class="btn btn-outline-danger" href="{{route('refuse_demission',$absence->id)}}">
                            Refusé
                         </a>

                        @endif

                    </td>
              </tr>
            @endif
          @endforeach


      </tfoot>
    </table>
  </div>
