<!-- /.card-header -->
<div class="card-body">
    <!-- form start -->
    {{-- @include('flash::message') --}}
   <form method="POST" enctype="multipart/form-data" action="{{route('sanctions.store')}}" id="form1">
       @csrf
       <div class="card-body">
           <div class="row">
               <div class="col-sm-6">
                   <div class="form-group">
                       <label for="exampleInputEmail1">Employé(e)</label>
                       <select name="employe_id" data-placeholder="Select employé" class="form-control form-control-select2 required" data-fouc>
                           <option value="-1" selected="selected">Selectionnez un employé</option>
                                   @foreach ($employes as $employe)
                                   <option value="{{$employe->id}}">{{$employe->nom}} {{$employe->prenom}}</option>
                                   @endforeach
                       </select>
                   </div>
               </div>
               <div class="col-sm-6">
                   <div class="form-group">
                       <label for="exampleInputEmail1">Motif</label>
                       <textarea name="motif" rows="1" class="form-control"></textarea>
                   </div>
               </div>
           </div>
           <div class="row">
               <div class="col-sm-6">
                   <div class="form-group">
                       <label for="">Date debut</label>
                       <div class="input-group date" id="reservationdate" data-target-input="nearest">
                           <input type="text"name="date_debut"id="reservationdate" class="form-control datetimepicker-input" data-target="#reservationdate"/>
                           <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                               <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                           </div>
                       </div>

                   </div>
               </div>
               <div class="col-sm-6">
                   <div class="form-group">
                       <label for="">Date fin:</label>
                       <div class="input-group date" id="reservationdatetwo" data-target-input="nearest">
                           <input type="text"name="date_fin" id="reservationdatetwo" class="form-control datetimepicker-input" data-target="#reservationdatetwo"onkeyup="calculateDifference()"/>
                           <div class="input-group-append" data-target="#reservationdatetwo" data-toggle="datetimepicker">
                               <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                           </div>
                       </div>

                   </div>
               </div>
               <div class="col-sm-6">
                <div class="form-group">
                    <label for="">Date a laquelle la faute a ete comise:</label>
                    <div class="input-group date" id="reservationdatethree" data-target-input="nearest">
                        <input type="text"name="Date_de_comise_la_faute" id="reservationdatethree" class="form-control datetimepicker-input" data-target="#reservationdatethree"onkeyup="calculateDifference()"/>
                        <div class="input-group-append" data-target="#reservationdatethree" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>

                </div>
            </div>
               <div class="col-sm-4">
                   <div class="form-group">
                       <label for="">Type de sanction</label>
                       <select name="type_sanction_id" data-placeholder="" class="form-control form-control-select2 required" data-fouc>
                           <option value="-1" selected="selected">Selectionnez le type de sanction</option>
                           @foreach ($type_sanctions as $type_sanction)
                           <option value="{{$type_sanction->id}}">{{$type_sanction->libelle}}</option>
                           @endforeach
                       </select>
                   </div>
               </div>
           </div>

           <!-- /.card-body -->

           <div class="card-footer">
           <button type="submit" class="btn btn-primary">Enregistrer</button>
           </div>
       </div>
   </form>

</div>
<!-- /.card-body -->
