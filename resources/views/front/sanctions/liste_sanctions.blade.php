 <!-- /.card-header -->
 <div class="card-body">
    <table id="example1" class="table table-bordered table-striped">
      <thead>
      <tr>
          <th>Nom et prenoms</th>
          <th>Type de sanction</th>
          <th>Motif</th>
          <th>Date début</th>
          <th>Date fin</th>
          <th class="text-center">Status</th>
          <th>decision</th>
          <th class="text-center">Etat</th>
          <th class="text-center">Action</th>

      </tr>
      </thead>
      <tbody>
        @foreach ($sanctions as $sanction )
            <tr>
                    {{-- <td class="text-center"><img class="rounded img-fluid avatar-40"src="../assets/images/user/01.jpg" alt="profile"></td> --}}
                <td>
                    @php
                        $employes = App\Models\Employe::where('id',$sanction->employe_id)->find($sanction->employe_id);
                        if($employes===null){
                            echo 'Le nom a été supprimé';
                        }else{
                            echo "$employes->nom $employes->prenom";
                        }
                    @endphp

                <td>{{$sanction->type_sanction->libelle}}</td>
                <td>{{$sanction->Motif}}</td>
                <td>{{$sanction->date_debut}}</td>
                <td>{{date('d/m/Y',strtotime($sanction->date_fin))}}</td>
                <td>
                    @if($sanction->is_motif_refus ==0)
                        <div class="d-flex align-items-center list-user-action">
                        <a class="badge badge-warning" data-toggle="tooltip" data-placement="top" title=""
                            data-original-title="En attente" href="#">
                            En attente
                            {{-- <i class="mr-0 ri-user-add-line"></i> --}}
                        </a>
                        @elseif($sanction->is_motif_refus ==1)

                        <a class="badge badge-success" data-toggle="tooltip" data-placement="top" title=""
                            data-original-title="Accepté" href="#">
                            Accepté
                            {{-- <i class="mr-0 ri-pencil-line"></i> --}}
                        </a>
                        @else
                        {{-- <a class="btn btn-sm bg-primary" data-toggle="tooltip" data-placement="top" title=""
                            data-original-title="Delete" href="#">
                            <i class="mr-0 ri-delete-bin-line"></i>
                            </a> --}}
                            <a class="badge badge-danger" data-toggle="tooltip" data-placement="top" title=""
                            data-original-title="Refusé">
                                Refusé
                                {{-- <i class="mr-0 ri-delete-bin-line"></i> --}}

                            </a>
                        </div>
                    @endif
                </td>
                <td>

                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-lg2{{$sanction->id}}">
                            <span style="">decision de sanction</span>
                        </button>
                      {{-- </div> --}}



                     {{-- MODAL1 Absence non justifié --}}
                     <div class="modal fade" id="modal-lg2{{$sanction->id}}">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h4 class="modal-title">
                                {{-- @php
                                    $employes = App\Models\Employe::where('id',$sanction->employe_id)->find($sanction->employe_id);
                                    if($employes===null){
                                        echo 'Le nom a été supprimé';
                                    }else{
                                        echo "$employes->nom $employes->prenom "."veuillez vous justifier";
                                    }
                                @endphp --}}
                              </h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <p>
                                <form method="POST" enctype="multipart/form-data" class="wizard-form" action="{{route('justify.index',$sanction->id)}}">
                                    @csrf
                                    {{-- @method('put') --}}
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="">Etablissez une decision de sanction</label>
                                                <textarea required name="decisionSanction" class="form-control" id="summernote" placeholder=""></textarea>
                                            </div>
                                        </div>

                                        {{-- <div class="col-sm-2">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Selectionner</label>
                                                <input type="file" name="rapport_explication" class="form-control" id="mobno" placeholder="Remplissez cette fiche de demande d'explication pour justifier votre absence du {{$absence->date_absence}} ">
                                            </div>
                                        </div> --}}
                                    </div>

                                    <div class="modal-footer justify-content-between">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                                        <button type="submit" class="btn btn-primary">Etablir</button>
                                      </div>
                                </form>
                              </p>
                            </div>

                          </div>
                          <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                      </div>
                      <!-- /.modal -->
                </td>
                <td>

                </td>
                <td>
                    <div class="d-flex align-items-center list-user-action">
                        <form class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title=""
                        data-original-title="Delete" method="POST" action="{{route('sanctions.destroy',$sanction->id)}}">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class=""><i class="far fa-trash-alt"></i></button>
                        </form>
                    </div>

                </td>
            </tr>
        @endforeach


      </tfoot>
    </table>
  </div>
  <!-- /.card-body -->
