@extends('layouts.pages.main')

@section('content')

  <!-- Navbar -->
@include('layouts.pages.header')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->

@include('layouts.pages.sidebar')
    <div class="content-wrapper">
            <section class="content">
            <div class="container-fluid">
                            <div class="mb-2 row">
                                <div class="col-sm-6">
                                    <h1>Details formations </h1>
                                </div>
                                <div class="col-sm-6">
                                    <a class="float-right btn btn-default"
                                    href="{{ route('formations.index') }}">
                                        Retour
                                    </a>
                                </div>
                            </div>

                    <div class="px-3 content">
                        <div class="card">

                            <div class="card-body">
                                <div class="row">
                                    @include('layouts.training.formation.show_fields')
                                </div>
                            </div>

                        </div>
                    </div>
            </div>
            </section>
    </div>
@endsection
