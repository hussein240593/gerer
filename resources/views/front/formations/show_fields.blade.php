<!-- Path Field -->
<div class="col-sm-12">
    <label>identifiant</label>
    <p>{{ $formations->id }}</p>
</div>

<!-- Offer Field -->
<div class="col-sm-12">
 <label>Designation</label>
    <p>{{ $formations->designation}}</p>
</div>

<!-- Content Field -->
<div class="col-sm-12">
    <label>Type de formation</label>
    <p>{{ $formations->type_formation }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    <label>Date debut</label>
    <p>{{ $formations->date_debut }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    <label>Date fin</label>
    <p>{{ $formations->date_fin }}</p>
</div>
<!-- Updated At Field -->
<div class="col-sm-12">
    <label>Formateur</label>
    <p>{{ $formations->formateur }}</p>
</div>

