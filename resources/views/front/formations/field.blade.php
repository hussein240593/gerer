<section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
<!-- Main content -->

      <div class="col-12">
        <div class="card">

          <!-- /.card-header -->
          <div class="card-body">
               <!-- /.card -->
          <!-- Horizontal Form -->
          <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Créer une formation</h3>
                <h3 class="float-right card-title"><a href="{{route('formations.index')}}">Retour</a></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="{{route('formations.store')}}" class="form-horizontal" method="POST" enctype="multipart/from-data">
                @csrf
                <div class="card-body">

                    {{-- designation field --}}

                  <div class="form-group row">
                    {{--liste employés field --}}

                    <div class="col-sm-12">
                      <label for="designation" class="col-form-label">Designation</label>
                      <input type="text" name="designation" class="form-control" id="designation" placeholder="Designation">
                  </div>


                </div>

                  <div class="form-group row">
                      {{-- sexe --}}
                      <div class="col-sm-6">
                          <label for="typeFormation" class="col-form-label">Type de formation</label>
                          <select class="form-control" name="type_formation">
                              <option value="">Veuillez sélectionnez --</option>
                              <option value="Laravel"> Laravel</option>
                              <option value="Angular"> Angular</option>
                          </select>
                      </div>
                      {{-- name --}}
                      <div class="col-sm-6">
                          <label for="date_debut" class="col-form-label">Date début</label>
                          <input type="text" name="date_debut" id="date_debut" class="form-control"placeholder="20/12/2020">
                      </div>

                  </div>

                  <div class="form-group row">
                      {{--liste employés field --}}
                      <div class="col-sm-6">
                          <label for="formateur" class="col-form-label">Formateur</label>
                          <select class="form-control" id="formateur" name="formateur">
                              <option value="">Veuillez sélectionnez --</option>
                              <option value="Hussein"> Hussein</option>
                          </select>
                      </div>
                      <div class="col-sm-6">
                        <label for="date_fin" class="col-form-label">Date fin</label>
                        <input type="text" name="date_fin" class="form-control" id="date_fin" placeholder="20/04/2021">
                    </div>

                  </div>

                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <div class="container">
                       <div class="rows" >

                          <button type="reset" class="btn btn-danger"> Annuler</button>
                          <button type="submit" class="btn btn-info">Créer la formation</button>

                       </div>
                      </div>

                </div>
                <!-- /.card-footer -->
              </form>
            </div>
            <!-- /.card -->

          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->

<!-- /.content -->
      <!-- /.row -->
      <!-- Main row -->

      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
