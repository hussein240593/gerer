@extends('main')

@section('content')



 <!-- Wrapper Start -->
 <div class="wrapper"
 style="background: url(../assets/images/background.png); background-attachment: fixed; background-size: cover; ">
	<!-- Main sidebar -->
    @include('layouts.sidebar')
    <!-- /main sidebar -->
	<!-- Main navbar -->
    @include('layouts.header')
	<!-- /main navbar -->

    <div class="content-page">
        <div class="container-fluid">
            <div class="row">

               <div class="col-xl-12 col-lg-12">
                     <div class="card">
                        <div class="card-header d-flex justify-content-between">
                           <div class="header-title">
                              <h4 class="card-title">Editer une formation</h4>
                           </div>
                        </div>
                        <div class="card-body">
                           <div class="new-user-info">
                                <form method="POST" enctype="multipart/form-data" class="wizard-form" action="{{route('formations.update',$formation->id)}}" data-fouc>
                                    @csrf
                                    @method('put')
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="fname">Libelle:</label>
                                            <input type="text"name="libelle"value="{{$formation->libelle}}" class="form-control" id="nom" placeholder="Libelle">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="lname">Date debut:</label>
                                            <input type="text" value="{{$formation->date_debut}}" name="date_debut" class="form-control" id="prenom" placeholder="Date debut">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="add1">Date fin:</label>
                                            <input type="text"value="{{$formation->date_fin}}" name="date_fin" class="form-control" id="add1" placeholder="Date fin">
                                        </div>
                                        <div class="form-group col-md-6">
                                        <label for="add2">Employé(e):</label>
                                        <select name="employe_id" data-placeholder="Veuillez sélectionnez --" class="form-control form-control-select2" data-fouc>
                                            {{-- <option value="">Selectionnez un employé </option> --}}
                                            @foreach ($employes as $employe)
                                            {{-- <optgroup label="Employé"> --}}
                                                <option value="{{$employe->id}}"> {{$employe->nom}}</option>
                                            {{-- </optgroup> --}}
                                            @endforeach
                                        </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                        <label for="add2">Type de formation:</label>
                                        <select name="type_formation_id" data-placeholder="Select un type formation" class="form-control form-control-select2" data-fouc>
                                            @foreach ($types_formations as $type_formation)
                                            {{-- <optgroup label="Type de formation"> --}}
                                                <option value="{{$type_formation->id}}">{{$type_formation->libelle}}</option>
                                            {{-- </optgroup> --}}
                                            @endforeach
                                        </select>
                                        </div>



                                        <div class="form-group col-md-12">
                                            <label for="mobno">Description:</label>
                                            <textarea type="text"value="{{$formation->description}}" name="description" class="form-control" id="mobno" placeholder="Description"></textarea>
                                        </div>

                                        <button type="submit" class="btn btn-success">Enregistrer</button>
                              </form>
                           </div>
                        </div>
                     </div>
               </div>
            </div>
         </div>

    </div>
 <!-- Page end  -->
 @include('layouts.footer')
</div>
<!-- Wrapper End-->
	<!-- /page content -->
@endsection
