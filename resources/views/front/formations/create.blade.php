@extends('layouts.main2')
@section('content')


  <!-- Navbar -->
    @include('layouts.header')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->

  @include('layouts.sidebars')
  <!-- Content Wrapper. Contains page content -->
 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="mb-2 row">
          <div class="col-sm-6">
            <h1 class="m-0">Tableau de bord</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Employés</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->

    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        {{-- @include('layouts.dashboard') --}}
        <!-- /.row -->
        <!-- Main row -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
        <div class="row">
            <div class="col-12">


            <div class="card card-info">
                <div class="card-header">
                <h3 class="card-title">Formulaire de formation</h3>
                <span class="breadcrumb float-sm-right"><a href="{{route('employes.create')}}">Retour</a></span>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                     <!-- form start -->
              <form method="POST" enctype="multipart/form-data" action="{{route('formations.store')}}" id="form1">
                @csrf
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Libelle</label>
                                <input type="text" name="libelle" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                              </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Date debut:</label>
                                <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                    <input type="text" name="date_debut" id="reservationdate" class="form-control datetimepicker-input" data-target="#reservationdate"/>
                                    <div class="input-group-append" data-target="#reservationdate"  data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                              </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Date fin:</label>
                                <div class="input-group date" id="reservationdatetwo" data-target-input="nearest">
                                    <input type="text" name="date_fin" id="reservationdatetwo" class="form-control datetimepicker-input" data-target="#reservationdatetwo" onkeyup="calculateDifference()"/>
                                    {{-- onkeyup="calculateDifference();" --}}
                                    <div class="input-group-append" data-target="#reservationdatetwo"  data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                              </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Employé(e):</label>
                                <select name="employe_id" data-placeholder="Veuillez sélectionnez --" class="form-control form-control-select2" data-fouc>
                                    {{-- <option value="">Selectionnez un employé </option> --}}
                                    @foreach ($employes as $employe)
                                    {{-- <optgroup label="Employé"> --}}
                                        <option value="{{$employe->id}}"> {{$employe->nom}}</option>
                                    {{-- </optgroup> --}}
                                    @endforeach
                                </select>
                              </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Type de formation:</label>
                                <select name="type_formation_id" data-placeholder="Select un type formation" class="form-control form-control-select2" data-fouc>
                                    @foreach ($types_formations as $type_formation)
                                    {{-- <optgroup label="Type de formation"> --}}
                                        <option value="{{$type_formation->id}}">{{$type_formation->libelle}}</option>
                                    {{-- </optgroup> --}}
                                    @endforeach
                                </select>
                              </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Description</label>
                                <textarea type="text" name="description" class="form-control" id="mobno" placeholder="Description"></textarea>
                              </div>
                        </div>


                        {{-- <div class="col-sm-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nombre de jour</label>
                                <input type="text" name="number_of_day" class="form-control" id="nmbreJour" placeholder=""readonly>
                              </div>
                        </div> --}}
                    </div>

                  <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Check me out</label>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  {{-- footer --}}
  @include('layouts.footer')
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->


@endsection
