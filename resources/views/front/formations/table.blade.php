<table id="example2" class="table table-bordered table-hover">
    <thead>
    <tr>
      <th>#</th>
      <th>Désignation</th>
      <th>Type formation</th>
      <th>Date début formation</th>
      <th>Date fin formation</th>
      <th>Formateur</th>
      <th>Action</th>
    </tr>
    </thead>
    <tbody>
        @foreach ($liste_formations as $formation)
            <tr>
                <td>{{$formation->id}}</td>
                <td>{{$formation->designation}}</td>
                <td>{{$formation->type_formation}}</td>
                <td>{{$formation->date_debut}}</td>
                <td>{{$formation->date_fin}}</td>
                <td>{{$formation->formateur}}</td>
                <td class="text-right project-actions">
                    {{-- <a class="btn btn-default btn-sm" href="#" title="Rapport">
                        <i class="fas fa-folder">
                        </i>

                    </a> --}}

                    <a class="btn btn-primary btn-sm" href="{{route('formations.show',$formation->id)}}" title="Voir les details">
                        <i class="fas fa-eye">
                        </i>

                    </a>
                    <a class="btn btn-info btn-sm" href="#" title="Modifier">
                        <i class="fas fa-pencil-alt">
                        </i>

                    </a>
                    <form action="{{route('formations.destroy',$formation->id)}}" method="POST">
                        @csrf
                        @method('DELETE')

                            <button type="submit" class="btn btn-danger btn-sm">
                                <i class="fas fa-trash"></i>
                            </button>


                    </form>
                </td>
            </tr>
        @endforeach
    </tbody>

  </table>
