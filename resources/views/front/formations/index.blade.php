@extends('layouts.main')
@section('content')


  <!-- Navbar -->
    @include('layouts.header')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->

  @include('layouts.sidebars')
  <!-- Content Wrapper. Contains page content -->
 <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="mb-2 row">
          <div class="col-sm-6">
            <h1 class="m-0">Tableau de bord</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Employés</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
  
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-info">
                        <div class="card-header">
                        <h3 class="card-title">Liste des formations</h3>
                        <span class="breadcrumb float-sm-right"><a href="{{route('formations.create')}}">Ajouter</a></span>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Employé</th>
                                <th>Libelle</th>
                                <th>Type formation</th>
                                <th>Date début formation</th>
                                <th>Date fin formation</th>
                                <th>Nombre de jour</th>

                                <th>Status</th>
                            <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($liste_formations as $formation)
                                <tr>
                                    <td>
                                        @php
                                            $employes = App\Models\Employe::where('id',$formation->employe_id)->find($formation->employe_id);
                                            if($employes===null){
                                                echo 'Employé a été supprimé';
                                            }else{
                                                echo $employes->nom;
                                            }
                                        @endphp
                                    </td>
                                    <td>{{$formation->libelle}}</td>
                                    <td>
                                        @php
                                            $typeFormation = App\Models\Type_formation::where('id',$formation->type_formation_id)->find($formation->type_formation_id);
                                            if($typeFormation===null){
                                                echo 'Le type de formation a été supprimé';
                                            }else{
                                                echo $typeFormation->libelle;
                                            }
                                        @endphp
                                    </td>
                                    <td>{{$formation->date_debut}}</td>
                                    <td>{{date('d/m/Y',strtotime($formation->date_fin))}}</td>
                                    <td>
                                        @php
                                            $date1 = date('Y-m-d');
                                            $date2 = $formation->date_fin;
                                            $date = strtotime($date2) - strtotime( $date1);
                                            $nmbrJ = $date/86400;
                                                if($nmbrJ>=0){

                                                        echo "<span class='badge badge-success'>".$nmbrJ."</span> ";

                                                            } elseif ($nmbrJ<0) {
                                                            echo "<span class='badge badge-danger'>0</span> ";
                                                            }
                                        @endphp
                                    </td>
                                    <td>
                                        @php
                                            $date1 = date('Y-m-d');
                                            $date2 = $formation->date_fin;
                                            $date = strtotime($date2) - strtotime( $date1);
                                            $nmbrJ = $date/86400;
                                                if($nmbrJ>=0){

                                                    echo "<span class='badge badge-success'>"."En cours"."</span> ";

                                                        } elseif ($nmbrJ<0) {
                                                        echo "<span class='badge badge-danger'>Fin</span> ";
                                                    }
                                        @endphp
                                    </td>


                                    <td>
                                        <div class="d-flex align-items-center list-user-action">
                                        <a class="mr-1 btn btn-sm bg-info" data-toggle="tooltip" data-placement="top" title=""
                                            data-original-title="Voir profil" href="{{route('formations.show',$formation->id)}}">
                                            <i class="far fa-eye"></i>
                                            </a>
                                        <a class="mr-1 btn btn-sm bg-secondary" data-toggle="tooltip" data-placement="top" title=""
                                            data-original-title="Edit" href="{{route('formations.edit',$formation->id)}}"><i class="far fa-edit"></i></a>
            
                                            <form class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title=""
                                            data-original-title="Delete" method="POST" action="{{route('formations.destroy',$formation->id)}}">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class=""><i class="far fa-trash-alt"></i></button>

                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach


                            </tfoot>
                        </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
  <!-- /.content-wrapper -->
  {{-- footer --}}
  @include('layouts.footer')
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

@endsection
