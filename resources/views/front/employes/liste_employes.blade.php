 <!-- /.card-header -->
 <div class="card-body">
    <table id="example1" class="table table-bordered table-striped">
      <thead>
      <tr>
        {{-- <th>Profil</th> --}}
        <th>Profil</th>
        <th>Nom complet</th>
        <th>Email</th>
        <th>Fonction(s)</th>
        <th>Departement</th>
        <th>Téléphone</th>
        <th>Status</th>
        <th>Demission</th>
        <th>Action</th>
      </tr>
      </thead>
      <tbody>


        @foreach($employes as $employe)

                {{-- {{dd($employe->absence->last()->id)}} --}}
                {{-- $employe->absence->last()->is_etat_accepte_demission==1 --}}
                            {{-- @if($employe->absence->last()->is_etat_accepte_demission ?? '' ) --}}

                {{-- <tr>
                    <td colspan="5">Cet employé ne fait plus partir de l'entreprise</td>

                </tr> --}}

                {{-- @elseif($employe->absence== null && $employe->absence->last()->is_etat_accepte_demission!=1) --}}


                <tr>
                    {{-- <td>{{dd($employe->absence->first()->is_etat_accepte_demission)}}</td> --}}
                    <td class="text-center"><a href="images/avatars/{{($employe->dossier_employe->first()->avatar  ?? '')}}"><img class="rounded img-fluid avatar-40" src="{{asset('images/avatars')}}/{{($employe->dossier_employe->first()->avatar ?? '')}}"style="width: 20px" alt="Aucun"></a></td>
                    <td>{{$employe->nom}} {{$employe->prenom}}</td>
                    <td><a class="__cf_email__" data-cfemail="6d000c1f0a080c1f04190c2d0a000c0401430e0200" href="mailto:{{$employe->email}}">{{$employe->email}}</a></td>
                    <td>
                        {{$employe->fonction->libelle}}
                    </td>
                    <td>
                        {{$employe->departement->nom}}
                    </td>
                    <td>{{$employe->telephone}}</td>
                    <td>
                        @if($employe->date_demission)
                        <span>En cours de demission</span>
                        @else
                        <span class="btn btn-success">En activité</span>
                        @endif
                    </td>
                    {{-- ACTION --}}
                    <td>
                        @if($employe->date_demission)
                        <a href="#">Accepté</a>
                        <a href="#">Refuser</a>
                        @else
                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-lg{{$employe->id}}">
                            Demissionner
                          </button>
                          <div class="modal fade" id="modal-lg{{$employe->id}}">
                            <div class="modal-dialog modal-lg">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h4 class="modal-title">{{$employe->id}}</h4>

                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">

                                    <form method="GET" class="form-horizontal" action="{{route('demission',$employe->id)}}">
                                        @csrf
                                        <div class="form-group row">
                                        <label for="inputName" class="col-sm-2 col-form-label">Date de demission</label>
                                        <div class="col-sm-10">
                                            <div class="input-group date" id="reservationdatetime" data-target-input="nearest">
                                                <input
                                                type="text"
                                                name="date_demission"
                                                class="form-control datetimepicker-input @error('date_demission') is-invalid @enderror"  value="{{ old('date_demission') }}" required autocomplete="date_demission"
                                                data-target="#reservationdatetime"/>
                                                @error('date_demission')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                                <div class="input-group-append" data-target="#reservationdatetime" data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                </div>
                                            </div>

                                        </div>
                                        </div>
                                        {{-- <div class="form-group row">
                                        <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" id="inputEmail" placeholder="Email">
                                        </div>
                                        </div> --}}
                                        {{-- <div class="form-group row">
                                        <label for="inputName2" class="col-sm-2 col-form-label">Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="inputName2" placeholder="Name">
                                        </div>
                                        </div> --}}
                                        <div class="form-group row">
                                        <label for="inputExperience" class="col-sm-2 col-form-label">Lettre de demission</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control"name="motif" id="inputExperience" placeholder="Motif"></textarea>
                                        </div>
                                        </div>
                                        <div class="form-group row">
                                        {{-- <label for="inputSkills" class="col-sm-2 col-form-label">Skills</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="inputSkills" placeholder="Skills">
                                        </div>
                                        </div> --}}
                                        <div class="form-group row">
                                        <div class="offset-sm-2 col-sm-10">
                                            {{-- <div class="checkbox">
                                            <label>
                                                <input type="checkbox"> I agree to the <a href="#">terms and conditions</a>
                                            </label>
                                            </div> --}}
                                        </div>
                                        </div>
                                        <div class="form-group row">
                                        <div class="offset-sm-2 col-sm-10">
                                            <button type="submit" class="btn btn-danger">Enregistrer</button>
                                        </div>
                                        </div>
                                    </form>





                                </div>
                                <div class="modal-footer justify-content-between">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn btn-primary">Save changes</button>
                                </div>
                              </div>
                              <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                          </div>
                          <!-- /.modal -->
                          @endif
                    </td>
                    <td>
                        <div class="d-flex align-items-center list-user-action">
                            {{-- Modal --}}
                            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-xl{{$employe->id}}">
                                <i class="far fa-eye"></i>
                            </button>
                            <div class="modal fade" id="modal-xl{{$employe->id}}">
                                <div class="modal-dialog modal-xl">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <h4 class="modal-title">Profil de {{$employe->nom}}</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>
                                    <div class="modal-body">
                                        <!-- Main content -->
                                        <section class="content">
                                            <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-md-3">

                                                    <div class="pt-0 card-body">
                                                        <div class="row">
                                                          <div class="col-7">
                                                            <h2 class="lead"><b>{{$employe->nom ?? ''}} {{$employe->prenom ?? ''}}</b></h2>
                                                            <p class="text-sm text-muted"><b>Fonction: </b> {{$employe->fonction->libelle ?? ''}}</p>
                                                            <ul class="mb-0 ml-4 fa-ul text-muted">
                                                              <li class="small"><span class="fa-li"><i class="fas fa-lg fa-envelope"></i></span> {{$employe->email ?? ''}}</li>
                                                              <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> {{$employe->telephone ?? ''}}</li>
                                                              <li class="small"><span class="fa-li"></span>Date de naissance: {{$employe->birthday ?? ''}}</li>
                                                              <li class="small"><span class="fa-li"></span>Sexe: {{$employe->sexe ?? ''}}</li>
                                                              <li class="small"><span class="fa-li"></span>Ville: {{$employe->ville->libelle ?? ''}}</li>
                                                            </ul>
                                                          </div>
                                                          <div class="text-center col-5">
                                                            <img src="images/avatars/{{($employe->dossier_employe->first()->avatar  ?? '')}}" class="img-circle img-fluid">
                                                          </div>
                                                        </div>
                                                      </div>
                                                </div>
                                                <!-- /.col -->
                                                <div class="col-md-9">
                                                    <div class="card card-info">
                                                        <div class="p-2 card-header">
                                                        <ul class="nav nav-pills">

                                                            <li class="nav-item"><a class="nav-link active" href="#timeline" data-toggle="tab">Infomation professionnelle</a></li>

                                                        </ul>
                                                        </div><!-- /.card-header -->
                                                        <div class="card-body">
                                                        <div class="tab-content">

                                                            <div class="tab-pane active" id="timeline">
                                                                <!-- Post -->
                                                            <div class="post">
                                                                <section class="content">
                                                                    <div class="container-fluid">
                                                                        <div class="row">
                                                                        <div class="col-12">

                                                                            <!-- Main content -->
                                                                            <div class="p-3 mb-3 invoice">
                                                                            <!-- title row -->
                                                                            <div class="row">
                                                                                <div class="col-12">
                                                                                <h4>

                                                                                    <small class="float-right">Date: {{ \Carbon\Carbon::parse(($employe->created_at ?? ''))->isoFormat('LLL')}}</small>
                                                                                </h4>
                                                                                </div>
                                                                                <!-- /.col -->
                                                                            </div>

                                                                            <!-- /.row -->

                                                                            <!-- Table row -->
                                                                            <div class="row">
                                                                                <div class="col-12 table-responsive">
                                                                                <table class="table table-striped">
                                                                                    <thead>
                                                                                    <tr>
                                                                                        <th>Departement</th>
                                                                                        <th>Type de fonction</th>
                                                                                        <th>Fonction</th>
                                                                                        <th>Type de piece</th>
                                                                                        <th>CV</th>
                                                                                        <th>Lettre de motivation</th>
                                                                                        <th>Adresse postale</th>
                                                                                    </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                        {{$employe->departement->nom ?? ''}}

                                                                                        </td>
                                                                                        <td>
                                                                                        {{$employe->type_fonction->libelle ?? ''}}

                                                                                        </td>
                                                                                        <td>
                                                                                        {{$employe->fonction->libelle ?? ''}}

                                                                                        </td>
                                                                                        <td>{{$employe->dossier_employe->first()->numero_piece  ?? ''}}</td>
                                                                                        {{-- download="pdf/{{($employe->dossier_employe->first()->cv)}}" --}}
                                                                                        <td><a href="pdf/{{($employe->dossier_employe->first()->cv  ?? '')}}" >CV.{{$employe->nom ?? ''}}</a></td>
                                                                                        <td><a href="pdf/{{($employe->dossier_employe->first()->lettre_motivation  ?? '')}}">LM.{{$employe->nom ?? ''}}</a></td>
                                                                                        <td>{{$employe->adr_postal ?? ''}}</td>
                                                                                    </tr>
                                                                                    </tbody>
                                                                                    </table>
                                                                                </div>
                                                                                <!-- /.col -->
                                                                            </div>
                                                                            <!-- /.row -->


                                                                            <!-- this row will not appear when printing -->
                                                                            <div class="row no-print">
                                                                                <div class="col-12">
                                                                                <a href="invoice-print.html" rel="noopener" target="_blank" class="btn btn-default"><i class="fas fa-print"></i> Télécharger</a>



                                                                                <button type="button" class="float-right btn btn-primary" style="margin-right: 5px;">
                                                                                    <i class="fas fa-"></i> Mise à jour
                                                                                </button>
                                                                                {{-- <a href="{{route('demissions')}}" class="float-right btn btn-default">
                                                                                    Demission
                                                                                </a> --}}

                                                                                </div>
                                                                            </div>
                                                                            </div>
                                                                            <!-- /.invoice -->
                                                                        </div><!-- /.col -->
                                                                        </div><!-- /.row -->
                                                                    </div><!-- /.container-fluid -->
                                                                    </section>
                                                                    <!-- /.content -->





                                                            </div>
                                                            <!-- /.post -->


                                                            </div>
                                                            <!-- /.tab-pane -->

                                                        </div>
                                                        <!-- /.tab-content -->
                                                        </div><!-- /.card-body -->
                                                    </div>
                                                <!-- /.card -->
                                                </div>
                                                <!-- /.col -->
                                            </div>
                                            <!-- /.row -->
                                            </div><!-- /.container-fluid -->
                                        </section>
                                        <!-- /.content -->
                                    </div>
                                    <div class="modal-footer justify-content-between">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                    {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->


                            {{-- <a class="badge badge-primary" data-toggle="tooltip" data-placement="top" title=""
                                data-original-title="Voir profil" href="{{route('employes.show',$employe->id)}}">
                                <i class="far fa-eye"></i>
                            </a> --}}
                            <a class="badge badge-warning" data-toggle="tooltip" data-placement="top" title=""
                                data-original-title="Edit" href="{{route('employes.edit',$employe->id)}}"><i class="far fa-edit"></i></a>
                            {{-- <a class="btn btn-sm bg-primary" data-toggle="tooltip" data-placement="top" title=""
                                data-original-title="Delete" href="#">
                                <i class="mr-0 ri-delete-bin-line"></i>
                            </a> --}}
                            <form class="badge badge-danger" data-toggle="tooltip" data-placement="top" title=""
                                data-original-title="Delete" method="POST" action="{{route('employes.destroy',$employe->id)}}">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class=""><i class="far fa-trash-alt"></i></button>

                            </form>
                        </div>
                    </td>


                </tr>

            {{-- @endif --}}


        @endforeach


      </tfoot>
    </table>
  </div>
  <!-- /.card-body -->
