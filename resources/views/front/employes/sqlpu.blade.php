@extends('layouts.main')
@section('content')

 @php
     try {
         $db = new \PDO('mysql:host=localhost;dbname=manager', 'root', '');
     } catch (\PDOException $e) {
         print "Erreur !: " . $e->getMessage() . "<br/>";
         die();
    }
 @endphp

  <!-- Navbar -->
    @include('layouts.header')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->

  @include('layouts.sidebar')
  <!-- Content Wrapper. Contains page content -->
 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="mb-2 row">
          <div class="col-sm-6">
            {{-- <h1 class="m-0">Tableau de bord</h1> --}}
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Accueil</a></li>
              <li class="breadcrumb-item active">Employés</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


 <!-- Main content -->
 <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">


          <div class="card card-info">
            <div class="card-header" style="background-color: #17a2b8 !important;">
              <h3 class="card-title">Liste des employés</h3>
              <span class="breadcrumb float-sm-right"><a href="{{route('employes.create')}}">Ajouter</a></span>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nom complet</th>
                  <th>Email</th>
                  <th>Fonction(s)</th>
                  <th>Departement</th>
                  <th>Téléphone</th>
                  <th>Situation</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>

                    

                    @foreach($employes as $employe)
                    @php
                      
                    $absence = $db->prepare('SELECT  nom,prenom,sexe,email,ville,telephone,numero_piece,is_etat_accepte_demission,employe_id FROM absences RIGHT JOIN employes ON absences.employe_id = employes.id WHERE is_etat_accepte_demission<>1 GROUP BY employe_id;
');
                  $absence->execute();
                  while($aff=$absence->fetch()){
                      $nom=$aff['nom'];
                      $prenom=$aff['prenom'];
                      $is_etat=$aff['is_etat_accepte_demission'];
                      $sexe=$aff['sexe'];
                      $email=$aff['email'];
                      $ville=$aff['ville'];
                      $telephone=$aff['telephone'];
                      $numero_piece=$aff['numero_piece'];
                      //$departement_id=$aff['departement_id'];
                             echo '
                             <tr>
                                <td>'.$nom.'</td>
                               <td>'.$email.'</td>
                               <td>'.$sexe.'</td>
                               <td>'.$ville.'</td>
                               <td>'.$telephone.'</td>
                               <td>'.$numero_piece.'</td>
                             </tr>
                             ';
  
                            
                           
                  }

                    @endphp
                    
                    <tr>
                        {{-- <td class="text-center"><img class="rounded img-fluid avatar-40"src="../assets/images/user/01.jpg" alt="profile"></td> --}}
                      
                        <td><a class="__cf_email__" data-cfemail="6d000c1f0a080c1f04190c2d0a000c0401430e0200" href="mailto:{{$employe->email}}">{{$employe->email}}</a></td>
                        <td>

                            @php
                                $fonction = App\Models\Fonction::where('id',$employe->id_fonction)->find($employe->id_fonction);
                                if($fonction===null){
                                    echo 'La fonction a été supprimé';
                                }else{
                                    echo $fonction->libelle;
                                }
                            @endphp
                        </td>
                        <td>
                            @php
                                $departement = App\Models\Departement::where('id',$employe->departement_id)->find($employe->departement_id);
                                if($departement===null){
                                    echo 'Le departement a été supprimé';
                                }else{
                                    echo $departement->nom_depart;
                                }
                            @endphp
                        </td>
                        <td>{{$employe->telephone}}</td>
                        <td>{{$employe->situation}}</td>

                        <td>
                            <div class="d-flex align-items-center list-user-action">
                               <a class="badge badge-primary" data-toggle="tooltip" data-placement="top" title=""
                                  data-original-title="Voir profil" href="{{route('employes.show',$employe->id)}}">
                                  <i class="far fa-eye"></i>
                                </a>
                               <a class="badge badge-warning" data-toggle="tooltip" data-placement="top" title=""
                                  data-original-title="Edit" href="{{route('employes.edit',$employe->id)}}"><i class="far fa-edit"></i></a>
                               {{-- <a class="btn btn-sm bg-primary" data-toggle="tooltip" data-placement="top" title=""
                                  data-original-title="Delete" href="#">
                                  <i class="mr-0 ri-delete-bin-line"></i>
                                </a> --}}
                                <form class="badge badge-danger" data-toggle="tooltip" data-placement="top" title=""
                                data-original-title="Delete" method="POST" action="{{route('employes.destroy',$employe->id)}}">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class=""><i class="far fa-trash-alt"></i></button>

                                </form>
                            </div>
                        </td>
                    </tr>
                    @endforeach


                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  {{-- footer --}}
  @include('layouts.footer')
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

@endsection
