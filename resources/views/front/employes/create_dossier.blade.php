<div class="card card-info">
    <div class="card-header">
    <h3 class="card-title">Créer un dossier employé</h3>
    {{-- <span class="breadcrumb float-sm-right"><a href="{{route('employes.index')}}">Retour</a></span> --}}
    </div>
    <!-- /.card-header -->
    <div class="card-body">
         <!-- form start -->
         <strong>les champs marqués en </strong><span class="text-danger">"*"</span><strong> sont obligatoire</strong>
         <hr>
  <form method="POST" enctype="multipart/form-data" action="{{route('dossier.store')}}">
    @csrf
    <div class="card-body">

        <div class="row">

            <div class="col-sm-4">
                <div class="form-group">
                    <label for="">Type pièce:</label>
                    <select name="type_piece" class="selectpicker form-control" data-style="py-0">
                        <option>Select type piece</option>
                        <option value="1">CNI</option>
                        <option value="2">ATTESTATION</option>

                     </select>
                  </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="">Numero de la pièce:</label>
                    <input type="text"name="number_of_piece" required class="form-control" id="" placeholder="Numero de pièce">
                  </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="">Photo</label>
                    <input type="file"name="avatar" required class="form-control" id="">
                  </div>
            </div>
        </div>
        <div class="row">

            <div class="col-sm-4">
                <div class="form-group">
                    <label  for="">CV <span class="text-danger">*</span></label>
                    <input type="file"name="cv"required class="form-control" id="">
                  </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="">Lettre de motivation <span class="text-danger">*</span></label>
                    <input type="file" name="lettre_motivation"required class="form-control" id="">
                  </div>
            </div>
             <div class="col-sm-4">
                <div class="form-group">
                    <label for="">Diplôme <span class="text-danger">*</span></label>
                    <input type="file" name="diplome"class="form-control @error('diplome') is-invalid @enderror" name="diplome" value="{{ old('diplome') }}" required autocomplete="diplome" autofocus>
                    @error('diplome')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Employé(e) <span class="text-danger">*</span></label>
                    <select class="form-control @error('employe_id') is-invalid @enderror" name="employe_id" value="{{ old('employe_id') }}" required autocomplete="employe_id" autofocus>
                        <option aria-selected value="-1">Veuillez sélectionnez l'employé(e)</option>
                        @foreach ($employes as $employe)
                            <option value="{{$employe->id}}"> {{$employe->nom}}</option>
                        @endforeach
                    </select>
                     @error('employe_id')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                      @enderror
                  </div>
            </div>
        </div>


      {{-- <div class="form-check">
        <input type="checkbox" class="form-check-input" id="exampleCheck1">
        <label class="form-check-label" for="exampleCheck1">Check me out</label>
      </div> --}}
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Enregistrer</button>
    </div>
  </form>

    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->
