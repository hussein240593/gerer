@extends('layouts.main')
@section('content')


  <!-- Navbar -->
    @include('layouts.header')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->

  @include('layouts.sidebar')
  <!-- Content Wrapper. Contains page content -->
 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="mb-2 row">
          <div class="col-sm-6">
            <h1 class="m-0">Tableau de bord</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Employés</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>150</h3>

                <p>New Orders</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>53<sup style="font-size: 20px">%</sup></h3>

                <p>Bounce Rate</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>44</h3>

                <p>User Registrations</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>65</h3>

                <p>Unique Visitors</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
        <div class="row">
            <div class="col-12">


            <div class="card card-info">
                <div class="card-header">
                <h3 class="card-title">Formulaire de recrutement</h3>
                <span class="breadcrumb float-sm-right"><a href="{{route('employes.index')}}">Retour</a></span>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                     <!-- form start -->
              <form method="POST" enctype="multipart/form-data" action="{{route('employes.update',$employe->id)}}">
                @csrf
                @method('put')
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nom</label>
                                <input type="text"name="nom"value="{{$employe->nom}}" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                              </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Prénoms</label>
                                <input type="text" name="prenom"value="{{$employe->prenom}}" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                              </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Adresse postale</label>
                                <input type="text" name="ad_postal"value="{{$employe->ad_postal}}" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                              </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email</label>
                                <input type="text"name="email"value="{{$employe->email}}" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                              </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Departement</label>
                                <select name="departement_id" data-placeholder="Select departement" class="form-control form-control-select2 required" data-fouc>
                                    <option selected="selected">Choisissez un departement</option>
                                    @foreach ($departements as $departement)
                                    <option value="{{$departement->id}}">{{$departement->nom_depart}}</option>
                                    @endforeach
                                </select>
                              </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Sexe</label>
                                <select name="sexe" class="selectpicker form-control" data-style="py-0">
                                    <option>Select sexe</option>
                                    <option>Homme</option>
                                    <option>Femme</option>

                                 </select>
                              </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Mois</label>
                                <select name="birth_month" data-placeholder="Day" class="form-control form-control-select2" data-fouc>
                                    <option>Select birth day</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="...">...</option>
                                    <option value="31">31</option>
                                </select>
                              </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Jour</label>
                                <select name="birth_day" data-placeholder="Day" class="form-control form-control-select2" data-fouc>
                                    <option>Select birth day</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="...">...</option>
                                    <option value="31">31</option>
                                </select>
                              </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Année</label>
                                <select name="birth_year" data-placeholder="Year" class="form-control form-control-select2" data-fouc>
                                    <option>Select birth year</option>
                                    <option value="1">1980</option>
                                    <option value="2">1981</option>
                                    <option value="3">1982</option>
                                    <option value="4">1983</option>
                                    <option value="5">1984</option>
                                    <option value="6">1985</option>
                                    <option value="7">1986</option>
                                    <option value="8">1987</option>
                                    <option value="9">1988</option>
                                    <option value="10">1989</option>
                                    <option value="11">1990</option>
                                </select>
                              </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Téléphone</label>
                                <input type="text"name="telephone"value="{{$employe->telephone}}" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                              </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Type pièce:</label>
                                <select name="type_piece" class="selectpicker form-control" data-style="py-0">
                                    <option>Select type piece</option>
                                    <option value="cni">CNI</option>
                                    <option value="attestation">ATTESTATION</option>

                                 </select>
                              </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Numero de la pièce:</label>
                                <input type="text"name="numero_piece"value="{{$employe->numero_piece}}" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                              </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Ville</label>
                                <select name="ville" class="selectpicker form-control" data-style="py-0">
                                    <option>Select Country</option>
                                    <option value="Gagnoa">Gagnoa</option>
                                    <option value="Abidjan">Abidjan</option>

                                 </select>
                              </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label  for="exampleInputEmail1">CV</label>
                                <input type="file"name="cv" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                              </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Lettre de motivation:</label>
                                <input type="file" name="lettre_motivation" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                              </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Type fonction:</label>
                                <select name="id_type_fonction" class="selectpicker form-control" data-style="py-0">
                                    <option>Select Country</option>
                                    @foreach ($types_fonctions as $type_fonction)
                                        <option value="{{$type_fonction->id}}">{{$type_fonction->libelle}}</option>
                                    @endforeach

                                 </select>
                              </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label  for="exampleInputEmail1">Fonction:</label>
                                <select name="id_fonction" class="selectpicker form-control" data-style="py-0">
                                    <option>Select Country</option>
                                    @foreach ($fonctions as $fonction)
                                        <option value="{{$fonction->id}}">{{$fonction->libelle}}</option>
                                    @endforeach

                                 </select>
                              </div>
                        </div>

                    </div>



                  <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Check me out</label>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  {{-- footer --}}
  @include('layouts.footer')
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

@endsection
