@extends('layouts.main')
@section('content')


  <!-- Navbar -->
    @include('layouts.header')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->

  @include('layouts.sidebars')
  <!-- Content Wrapper. Contains page content -->
 <!-- Content Wrapper. Contains page content -->

     <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="mb-2 row">
          <div class="col-sm-6">
            <h1>Profile</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Accueil</a></li>
              <li class="breadcrumb-item active">Profile</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="{{asset('images/avatars')}}/{{$dossierEmployes->avatar ?? ''}}"
                       alt="picture" style="width: 50px">
                </div>

                <h3 class="text-center profile-username">{{$employe->nom ?? ''}} {{$employe->prenom ?? ''}}</h3>

                <p class="text-center text-muted">
                    {{$employe->type_fonction->libelle ?? ''}}
                </p>

                <ul class="mb-3 list-group list-group-unbordered">
                  <li class="list-group-item">
                    <i class="mr-1 fas fa-book"></i><a class="float-right">{{$employe->email ?? ''}}</a>
                  </li>
                  <li class="list-group-item">
                    <i class="mr-1 fas fa-map-marker-alt"></i> <a class="float-right">{{$employe->telephone ?? ''}}</a>
                  </li>
                  {{-- <li class="list-group-item">
                    <b>Friends</b> <a class="float-right">13,287</a>
                  </li> --}}
                </ul>

                <a href="#" class="btn btn-success btn-block"><b>En activité</b></a>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Mise à jour</h3>
              </div>

            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="p-2 card-header">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Infomation personnelle</a></li>
                  <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Infomation professionnelle</a></li>
                  <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Settings</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                    <!-- Post -->
                    <div class="post">
                        <section class="content">
                            <div class="container-fluid">
                              <div class="row">
                                <div class="col-12">
                                  {{-- <div class="callout callout-info">
                                    <h5><i class="fas fa-info"></i> Note:</h5>
                                    This page has been enhanced for printing. Click the print button at the bottom of the invoice to test.
                                  </div> --}}


                                  <!-- Main content -->
                                  <div class="p-3 mb-3 invoice">
                                    <!-- title row -->
                                    <div class="row">
                                      <div class="col-12">
                                        <h4>
                                          {{-- <i class="fas fa-globe"></i> AdminLTE, Inc. --}}
                                          <small class="float-right">Date: {{ \Carbon\Carbon::parse(($employe->created_at ?? ''))->isoFormat('LLL')}}</small>
                                        </h4>
                                      </div>
                                      <!-- /.col -->
                                    </div>


                                    <!-- Table row -->
                                    <div class="row">
                                      <div class="col-12 table-responsive">
                                        <table class="table table-striped">
                                          <thead>
                                          <tr>
                                            <th>Nom</th>
                                            <th>Prenom</th>
                                            <th>Date de naissance</th>
                                            <th>CNI</th>
                                            <th>Sexe</th>
                                            <th>Telephone</th>
                                            <th>Ville</th>
                                          </tr>
                                          </thead>
                                          <tbody>
                                          <tr>
                                            <td>{{$employe->nom ?? ''}}</td>
                                            <td>{{$employe->prenom ?? ''}}</td>
                                            <td>
                                                {{ \Carbon\Carbon::parse(($employe->birthday ?? ''))->isoFormat('LLL')}}
                                                {{-- {{$employe->birthday}} --}}
                                            </td>
                                            <td>{{$dossierEmployes->number_of_piece ?? ''}}</td>
                                            <td>{{$employe->sexe ?? ''}}</td>
                                            <td>{{$employe->telephone ?? ''}}</td>
                                            <td>
                                                <td>{{$employe->ville->libelle ?? ''}}</td>

                                            </td>
                                          </tr>
                                          </tbody>
                                        </table>
                                      </div>
                                      <!-- /.col -->
                                    </div>
                                    <!-- /.row -->


                                    <!-- this row will not appear when printing -->
                                    <div class="row no-print">
                                      <div class="col-12">
                                        <a href="invoice-print.html" rel="noopener" target="_blank" class="btn btn-default"><i class="fas fa-print"></i> Print</a>
                                        <button type="button" class="float-right btn btn-success"><i class="far fa-credit-card"></i> Submit
                                          Payment
                                        </button>
                                        <button type="button" class="float-right btn btn-primary" style="margin-right: 5px;">
                                          <i class="fas fa-download"></i> Generate PDF
                                        </button>
                                      </div>
                                    </div>
                                  </div>
                                  <!-- /.invoice -->
                                </div><!-- /.col -->
                              </div><!-- /.row -->
                            </div><!-- /.container-fluid -->
                          </section>
                          <!-- /.content -->
                    </div>
                    <!-- /.post -->
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="timeline">
                      <!-- Post -->
                    <div class="post">
                        <section class="content">
                            <div class="container-fluid">
                              <div class="row">
                                <div class="col-12">

                                  <!-- Main content -->
                                  <div class="p-3 mb-3 invoice">
                                    <!-- title row -->
                                    <div class="row">
                                      <div class="col-12">
                                        <h4>

                                          <small class="float-right">Date: {{ \Carbon\Carbon::parse(($employe->created_at ?? ''))->isoFormat('LLL')}}</small>
                                        </h4>
                                      </div>
                                      <!-- /.col -->
                                    </div>

                                    <!-- /.row -->

                                    <!-- Table row -->
                                    <div class="row">
                                      <div class="col-12 table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                            <tr>
                                              <th>Departement</th>
                                              <th>Type de fonction</th>
                                              <th>Fonction</th>
                                              {{-- <th>Salaire</th> --}}
                                              <th>CV</th>
                                              <th>Lettre de motivation</th>
                                              <th>Adresse postale</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                              <td>
                                                {{$employe->departement->nom ?? ''}}

                                              </td>
                                              <td>
                                                {{$employe->type_fonction->libelle ?? ''}}

                                              </td>
                                              <td>
                                                {{$employe->fonction->libelle ?? ''}}

                                              </td>
                                              {{-- <td>{{$dossierEmployes->numero_piece}}</td> --}}
                                              <td><a href="{{$dossierEmployes->cv ?? ''}}" download="{{$dossierEmployes->cv ?? ''}}">CV.{{$employe->nom ?? ''}}</a></td>
                                              <td>{{$dossierEmployes->lettre_motivation ?? ''}}</td>
                                              <td>{{$employe->adr_postal ?? ''}}</td>
                                            </tr>
                                            </tbody>
                                          </table>
                                      </div>
                                      <!-- /.col -->
                                    </div>
                                    <!-- /.row -->


                                    <!-- this row will not appear when printing -->
                                    <div class="row no-print">
                                      <div class="col-12">
                                        <a href="invoice-print.html" rel="noopener" target="_blank" class="btn btn-default"><i class="fas fa-print"></i> Print</a>
                                        <button type="button" class="float-right btn btn-success"><i class="far fa-credit-card"></i> Submit
                                          Payment
                                        </button>
                                        <button type="button" class="float-right btn btn-primary" style="margin-right: 5px;">
                                          <i class="fas fa-download"></i> Generate PDF
                                        </button>
                                      </div>
                                    </div>
                                  </div>
                                  <!-- /.invoice -->
                                </div><!-- /.col -->
                              </div><!-- /.row -->
                            </div><!-- /.container-fluid -->
                          </section>
                          <!-- /.content -->





                    </div>
                    <!-- /.post -->


                  </div>
                  <!-- /.tab-pane -->

                  <div class="tab-pane" id="settings">
                    <form class="form-horizontal">
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Name</label>
                        <div class="col-sm-10">
                          <input type="email" class="form-control" id="inputName" placeholder="Name">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                          <input type="email" class="form-control" id="inputEmail" placeholder="Email">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputName2" class="col-sm-2 col-form-label">Name</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="inputName2" placeholder="Name">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputExperience" class="col-sm-2 col-form-label">Experience</label>
                        <div class="col-sm-10">
                          <textarea class="form-control" id="inputExperience" placeholder="Experience"></textarea>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputSkills" class="col-sm-2 col-form-label">Skills</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="inputSkills" placeholder="Skills">
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> I agree to the <a href="#">terms and conditions</a>
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-danger">Enregistrer</button>
                        </div>
                      </div>
                    </form>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <!-- /.content-wrapper -->
  {{-- footer --}}
  @include('layouts.footer')
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

@endsection

