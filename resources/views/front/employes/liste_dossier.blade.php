 <!-- /.card-header -->

 <div class="card-body">
     <form method="GET" action="{{route('employes.index')}}">
         <input type="text" id="search" name="search">
         <button>search</button>
     </form>



    <table id="example1" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th>Profil</th>
        <th>Nom complet</th>
        <th>CV</th>
        <th>lettre motivation</th>
        <th>Type de piece</th>
        <th>Numero de pièce</th>
        {{-- <th>Situation</th> --}}
        <th>Action</th>
      </tr>
      </thead>
      <tbody>


          @foreach($dossierEmployes as $dossier)
          {{-- @php
              $absence = App\Models\Absence::where('id',$employe->employe_id)->find($employe->employe_id);
              echo dd($absence->absences->nom);
          @endphp --}}
         {{-- @php

          $employe = App\Models\Employe::whereHas('absence',function($q){
              $q->where('is_etat_accepte_demission',1);
              dd($employe->absences->is_etat_accepte_demission);
          });

          @endphp --}}

          <tr>
              <td class="text-center"><a href="{{asset('images/avatars')}}/{{$dossier->avatar ?? ''}}"><img class="rounded img-fluid avatar-40" src="{{asset('images/avatars')}}/{{$dossier->avatar ?? ''}}"style="width: 50px" alt="Aucun"></a></td>
              <td>{{$dossier->employe->nom}} {{$dossier->employe->prenom}}</td>
              {{-- <td><a class="__cf_email__" data-cfemail="6d000c1f0a080c1f04190c2d0a000c0401430e0200" href="mailto:{{$dossier->employe->email}}">{{$dossier->employe->email}}</a></td> --}}
              <td>
                {{$dossier->cv}}
              </td>
              <td>
                {{$dossier->lettre_motivation}}
              </td>
              <td>{{$dossier->type_piece}}</td>
               <td>  {{$dossier->number_of_piece}}</td>
                {{-- @php
                    $absence = App\Models\Employe::find(2)->absence;
                    dd($absence);
                @endphp --}}
              {{-- <td>{{$employe->situation}}</td> --}}

              <td>
                  <div class="d-flex align-items-center list-user-action">
                      {{-- Modal --}}
                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-lg{{$dossier->id}}">
                        <i class="far fa-eye"></i>
                    </button>
                    <div class="modal fade" id="modal-lg{{$dossier->id}}">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h4 class="modal-title">Profil de {{$dossier->employe->nom}}</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <p>One fine body&hellip;</p>
                            </div>
                            <div class="modal-footer justify-content-between">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              <button type="button" class="btn btn-primary">Save changes</button>
                            </div>
                          </div>
                          <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                      </div>
                      <!-- /.modal -->


                     <a class="badge badge-primary" data-toggle="tooltip" data-placement="top" title=""
                        data-original-title="Voir profil" href="{{route('employes.show',$dossier->id)}}">
                        <i class="far fa-eye"></i>
                      </a>
                     <a class="badge badge-warning" data-toggle="tooltip" data-placement="top" title=""
                        data-original-title="Edit" href="{{route('employes.edit',$dossier->id)}}"><i class="far fa-edit"></i></a>
                     {{-- <a class="btn btn-sm bg-primary" data-toggle="tooltip" data-placement="top" title=""
                        data-original-title="Delete" href="#">
                        <i class="mr-0 ri-delete-bin-line"></i>
                      </a> --}}
                      <form class="badge badge-danger" data-toggle="tooltip" data-placement="top" title=""
                      data-original-title="Delete" method="POST" action="{{route('employes.destroy',$dossier->id)}}">
                          @csrf
                          @method('DELETE')
                          <button type="submit" class=""><i class="far fa-trash-alt"></i></button>

                      </form>
                  </div>
              </td>
          </tr>
          @endforeach


      </tfoot>
    </table>


  </div>
  <!-- /.card-body -->
