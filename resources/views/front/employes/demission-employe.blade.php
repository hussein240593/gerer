@extends('layouts.main2')
@section('content')


  <!-- Navbar -->
    @include('layouts.header')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->

  @include('layouts.sidebars')
  <!-- Content Wrapper. Contains page content -->
 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="mb-2 row">
          <div class="col-sm-6">
            <h1 class="m-0">Tableau de bord</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Employés</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->

    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        {{-- @include('layouts.dashboard') --}}
        <!-- /.row -->
        <!-- Main row -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
        <div class="row">
            <div class="col-12">


            <div class="card card-info">
                <div class="card-header">
                <h3 class="card-title">Formulaire de formation</h3>
                <span class="breadcrumb float-sm-right"><a href="{{route('employes.create')}}">Retour</a></span>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                     <!-- form start -->

                         {{-- DEMISSION --}}
                         <div class="tab-pane" id="settings">
                            <form method="POST" class="form-horizontal" action="{{route('demissions')}}">
                                @csrf
                                <div class="form-group row">
                                <label for="inputName" class="col-sm-2 col-form-label">Date de demission</label>
                                <div class="col-sm-10">
                                    <div class="input-group date" id="reservationdatetime" data-target-input="nearest">
                                        <input
                                        type="text"
                                        name="date_demission"
                                        class="form-control datetimepicker-input @error('date_demission') is-invalid @enderror"  value="{{ old('date_demission') }}" required autocomplete="date_demission"
                                        data-target="#reservationdatetime"/>
                                        @error('date_demission')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                        <div class="input-group-append" data-target="#reservationdatetime" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>

                                </div>
                                </div>
                                {{-- <div class="form-group row">
                                <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="inputEmail" placeholder="Email">
                                </div>
                                </div> --}}
                                {{-- <div class="form-group row">
                                <label for="inputName2" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputName2" placeholder="Name">
                                </div>
                                </div> --}}
                                <div class="form-group row">
                                <label for="inputExperience" class="col-sm-2 col-form-label">Lettre de demission</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control"name="motif" id="inputExperience" placeholder="Motif"></textarea>
                                </div>
                                </div>
                                <div class="form-group row">
                                {{-- <label for="inputSkills" class="col-sm-2 col-form-label">Skills</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputSkills" placeholder="Skills">
                                </div>
                                </div> --}}
                                <div class="form-group row">
                                <div class="offset-sm-2 col-sm-10">
                                    <div class="checkbox">
                                    <label>
                                        <input type="checkbox"> I agree to the <a href="#">terms and conditions</a>
                                    </label>
                                    </div>
                                </div>
                                </div>
                                <div class="form-group row">
                                <div class="offset-sm-2 col-sm-10">
                                    <button type="submit" class="btn btn-danger">Enregistrer</button>
                                </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.tab-pane -->

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  {{-- footer --}}
  @include('layouts.footer')
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->


@endsection
