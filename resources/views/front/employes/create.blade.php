<div class="card card-info">
    <div class="card-header">
    <h3 class="card-title">Créer un employé</h3>
    {{-- <span class="breadcrumb float-sm-right"><a href="{{route('employes.index')}}">Retour</a></span> --}}
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <strong>les champs marqués en </strong><span class="text-danger">"*"</span><strong> sont obligatoire</strong>
        <hr>
         <!-- form start -->
  <form method="POST" enctype="multipart/form-data" action="{{route('employes.store')}}">
    @csrf
    <div class="card-body">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="Nom">{{ __('Nom') }} <span class="text-danger">*</span></label>
                    <input type="text" class="form-control @error('nom') is-invalid @enderror" name="nom" id=""value="{{ old('nom') }}" required autocomplete="nom" autofocus>
                    @error('nom')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror

                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="Prénoms">{{ __('Prénoms') }} <span class="text-danger">*</span></label>
                    <input type="text" class="form-control @error('prenom') is-invalid @enderror" name="prenom" id=""value="{{ old('prenom') }}" required autocomplete="prenom" autofocus>
                    @error('prenom')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="Adresse postale">{{ __('Adresse postale') }} </label>
                    <input type="text" name="ad_postal" class="form-control" id="">
                  </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="">{{ __('Email') }} <span class="text-danger">*</span></label>
                    <input type="text" class="form-control @error('email') is-invalid @enderror" name="email" id=""value="{{ old('email') }}" required autocomplete="email" autofocus>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for=""> {{ __('Département') }} <span class="text-danger">*</span></label>
                    <select data-placeholder="Select departement" class="form-control @error('departement_id') is-invalid @enderror" name="departement_id" value="{{ old('departement_id') }}" required autocomplete="departement_id" autofocus data-fouc>
                        <option value="-1" selected="selected">Choisissez un departement</option>
                        @foreach ($departements as $departement)
                        <option value="{{$departement->id}}">{{$departement->nom}}</option>
                        @endforeach
                    </select>
                    @error('departement_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="Sexe"> {{ __('Sexe') }}</label>
                    <select name="sexe" class="selectpicker form-control" data-style="py-0">
                        <option value="-1">Selectionnez le sexe</option>
                        <option value="Masculin">Homme</option>
                        <option value="Feminin">Femme</option>

                     </select>
                  </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="">Date de naissance</label>
                    <div class="input-group date" id="reservationdate" data-target-input="nearest">
                    <input
                    type="text"
                    name="birthday"
                    class="form-control datetimepicker-input @error('birthday') is-invalid @enderror"  value="{{ old('birthday') }}" required autocomplete="birthday"
                    data-target="#reservationdate"/>
                    @error('birthday')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                    <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                    </div>

                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="Téléphone">{{ __('Téléphone') }}</label>
                    <input type="text"name="telephone" class="form-control" id="">
                  </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="Ville">{{ __('Ville') }} <span class="text-danger">*</span></label>
                    <select name="ville_id" data-placeholder="Select Ville" class="form-control form-control-select2 required" data-fouc>
                        <option value="-1" selected="selected">Choisissez une ville</option>
                        @foreach ($villes as $ville)
                        <option value="{{$ville->id}}">{{$ville->libelle}}</option>
                        @endforeach
                    </select>
                  </div>
            </div>
        </div>



        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="Type fonction">{{ __('Type fonction') }} <span class="text-danger">*</span></label>
                    <select class="form-control @error('type_fonction_id') is-invalid @enderror" name="type_fonction_id" value="{{ old('type_fonction_id') }}" required autocomplete="type_fonction_id" autofocus data-style="py-0">
                        <option>Selectionnez le type de fonction ...</option>
                        @foreach ($types_fonctions as $type_fonction)
                            <option value="{{$type_fonction->id}}">{{$type_fonction->libelle}}</option>
                        @endforeach

                     </select>
                    @error('type_fonction_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label  for="Fonction">{{ __('Fonction') }} <span class="text-danger">*</span></label>
                    <select class=" form-control @error('fonction_id') is-invalid @enderror" name="fonction_id" value="{{ old('fonction_id') }}" required autocomplete="fonction_id" autofocus data-style="py-0">
                        <option>Selectionnez la fonction ...</option>
                        @foreach ($fonctions as $fonction)
                            <option value="{{$fonction->id}}">{{$fonction->libelle}}</option>
                        @endforeach

                     </select>
                     @error('fonction_id')
                     <span class="invalid-feedback" role="alert">
                         <strong>{{ $message }}</strong>
                     </span>
                    @enderror
                    </div>
            </div>

        </div>



      {{-- <div class="form-check">
        <input type="checkbox" class="form-check-input" id="exampleCheck1">
        <label class="form-check-label" for="exampleCheck1">Check me out</label>
      </div> --}}
    </div>
    <!-- /.card-body -->

    <div class="card-footer" style="text-align:center">
      <button type="submit" class="btn btn-warning">Enregistrer</button>
    </div>
  </form>

    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->
