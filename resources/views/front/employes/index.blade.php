@extends('layouts.main')
@section('content')


  <!-- Navbar -->
    @include('layouts.header')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->

  @include('layouts.sidebars')

<div class="wrapper">



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">


    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">


        <div class="row">

          <div class="col-12 col-sm-12">
            <div class="card card-info card-tabs">
              <div class="p-0 pt-1 card-header">
                <ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">
                  <li class="px-3 pt-2"><h3 class="card-title">Gestion des employés</h3></li>
                  <li class="nav-item">
                    <a class="nav-link active" id="custom-tabs-two-home-tab" data-toggle="pill" href="#custom-tabs-two-home" role="tab" aria-controls="custom-tabs-two-home" aria-selected="true">Liste des employés</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-two-profile-tab" data-toggle="pill" href="#custom-tabs-two-profile" role="tab" aria-controls="custom-tabs-two-profile" aria-selected="false">Ajouter un employé</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-two-messages-tab" data-toggle="pill" href="#custom-tabs-two-messages" role="tab" aria-controls="custom-tabs-two-messages" aria-selected="false">Ajouter dossier employé</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-two-settings-tab" data-toggle="pill" href="#custom-tabs-two-settings" role="tab" aria-controls="custom-tabs-two-settings" aria-selected="false">Liste des dossiers</a>
                  </li>
                </ul>
              </div>
              <div class="card-body">
                <div class="tab-content" id="custom-tabs-two-tabContent">
                  <div class="tab-pane fade show active" id="custom-tabs-two-home" role="tabpanel" aria-labelledby="custom-tabs-two-home-tab">
                     {{-- Presents --}}
                      <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Employés</h3>
                            {{-- <span class="breadcrumb float-sm-right"><a href="{{route('conges.create')}}">Demande de congé</a></span> --}}
                        </div>
                            <!-- /.card-header -->
                            @include("front.employes.liste_employes")
                            <!-- /.card-body -->
                      </div>
                  </div>
                  <div class="tab-pane fade" id="custom-tabs-two-profile" role="tabpanel" aria-labelledby="custom-tabs-two-profile-tab">
                    @include("front.employes.create")
                            <!-- /.card-body -->
                  </div>
                  <div class="tab-pane fade" id="custom-tabs-two-messages" role="tabpanel" aria-labelledby="custom-tabs-two-messages-tab">
                    @include("front.employes.create_dossier")
                  </div>
                  <div class="tab-pane fade" id="custom-tabs-two-settings" role="tabpanel" aria-labelledby="custom-tabs-two-settings-tab">
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Dossiers</h3>
                        </div>
                    </div>
                    @include("front.employes.liste_dossier")
                  </div>
                </div>
              </div>
              <!-- /.card -->
            </div>
          </div>
        </div>



        <!-- /.card -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@include('layouts.footer')

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

@endsection

