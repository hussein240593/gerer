


            <div class="card card-info">
                <div class="card-header">
                <h3 class="card-title">Créer un pointage</h3>
                <span class="breadcrumb float-sm-right"><a href="{{route('absences.index')}}">Retour</a></span>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                     <!-- form start -->

              <form method="POST" enctype="multipart/form-data" action="{{route('absences.store')}}" id="form1">
                @csrf
                <div class="card-body">
                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Date de pointe</label>
                                <div class="input-group date" id="reservationdatetime" data-target-input="nearest">
                                  <input
                                  type="text"
                                  name="date_absence"
                                  class="form-control datetimepicker-input"@error('date_absence') is-invalid @enderror"  value="{{ old('date_absence') }}" required autocomplete="date_absence"
                                  data-target="#reservationdatetime"/>
                                   @error('date_absence')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                                  @enderror
                                  <div class="input-group-append" data-target="#reservationdatetime" data-toggle="datetimepicker">
                                      <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                  </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="">Employé(e)</label>
                                <select name="employe_id"  @error('employe_id') is-invalid @enderror  value="{{ old('employe_id') }}"  autocomplete="employe_id"
                                data-placeholder=""
                                class="form-control form-control-select2"
                                data-fouc>
                                    <option aria-selected value="-1">Veuillez sélectionnez l'employé(e)</option>
                                    @foreach ($employes as $employe)
                                        <option value="{{$employe->id}}"> {{$employe->nom}}</option>
                                    @endforeach
                                </select>
                                 @error('employe_id')
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                  </span>
                                  @enderror
                              </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="">Status</label>
                                <select required name="is_status_present" data-placeholder="" class="form-control form-control-select2" data-fouc>
                                    <option aria-selected value="-1">Veuillez sélectionnez le status</option>

                                        <option value="5">Demission</option>
                                        <option value="1">Absent</option>
                                        <option value="2">Présent</option>

                                </select>
                              </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Pointer</button>
                </div>
              </form>

                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->

