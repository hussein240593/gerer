<div class="card-body" style="box-shadow: hsla(226, 92%, 15%, 0.911) !important;">
    <table id="example1" class="table table-bordered table-striped">
      <thead>
          <tr class="ligth">
              <th>Nom et prenom</th>
              <th>date de présence</th>
              {{-- <th>Date fin</th> --}}

              <th>Status</th>
              {{-- <th>Actions</th> --}}

              {{-- <th class="min-width: 100px">Actions</th> --}}

           </tr>
      </thead>
      <tbody>
          @foreach($absences as $absence)
            @if ($absence->is_status_present==2)
              <tr>
                  <td>
                      @php
                          $employes = App\Models\Employe::where('id',$absence->employe_id)->find($absence->employe_id);
                          if($employes===null){
                              echo 'Le nom a été supprimé';
                          }else{
                              echo "$employes->nom $employes->prenom";
                          }
                      @endphp
                  </td>
                  <td>{{ \Carbon\Carbon::parse(($absence->date_absence))->isoFormat('LLL') }}</td>
                  {{-- <td>{{date('d/m/Y',strtotime($absence->date_absence))}}</td> --}}



                    <td><span class="badge badge-success">En activité</span></td>

              </tr>
            @endif
          @endforeach


      </tfoot>
    </table>
  </div>
