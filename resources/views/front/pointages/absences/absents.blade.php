<div class="card-body">
    <table id="example2" class="table table-bordered table-striped">
      <thead>
          <tr class="ligth">
              <th>Nom et prenom</th>
              <th>date absence</th>
              {{-- <th>Date fin</th> --}}

              <th>Etat</th>

              {{-- <th class="min-width: 100px">Actions</th> --}}

           </tr>
      </thead>
      <tbody>
          @foreach($absences as $absence)
            @if ($absence->is_status_present==1)


              <tr>
                <td>
                    {{$absence->employe->nom}} {{$absence->employe->prenom}}

                  </td>
                  <td>{{ \Carbon\Carbon::parse(($absence->date_absence))->isoFormat('LLL') }}</td>
                  {{-- <td>{{date('d/m/Y',strtotime($absence->date_absence))}}</td> --}}
                  {{-- Date: {{\Carbon\Carbon::now()->isoFormat('LL')}} --}}

                  <td>

                      {{-- <a href="" class="badge badge-danger" data-toggle="tooltip" data-placement="top" title=""
                          data-original-title="Non justifié">
                          Non justifié
                      </a> --}}
                      @if($absence->rapport_explication=="0")
                      {{-- <div class="card-body"> --}}
                        <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#modal-lg2{{$absence->id}}">
                            Non justifié
                          </button>
                      {{-- </div> --}}
                      @else
                      <div class="card-body">

                        <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#modal-lg{{$absence->id}}">
                            Justifié
                          </button>
                        </div>
                      @endif

                         {{-- MODAL1 Absence non justifié --}}
                      <div class="modal fade" id="modal-lg2{{$absence->id}}">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h4 class="modal-title">
                                {{$absence->employe->nom ?? 'Le nom a été supprimé'}} {{$absence->employe->prenom}}
                                veuillez vous justifier

                              </h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <p>
                                <form method="POST" enctype="multipart/form-data" class="wizard-form" action="{{route('justify.index',$absence->id)}}">
                                    @csrf
                                    {{-- @method('put') --}}
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="">Rapport</label>
                                                <textarea type="text" name="rapport_explication" class="form-control" id="summernote" placeholder="Remplissez cette fiche de demande d'explication pour justifier votre absence du {{$absence->date_absence}} "></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal-footer justify-content-between">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">fermer</button>
                                        <button type="submit" class="btn btn-primary">Justifier</button>
                                      </div>
                                </form>
                              </p>
                            </div>

                          </div>
                          <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                      </div>
                      <!-- /.modal -->


                      {{-- MODAL2 Absence justifié --}}
                      <div class="modal fade" id="modal-lg{{$absence->id}}">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="ribbon-wrapper ribbon-lg">
                                <div class="ribbon bg-info">
                                    {{$absence->employe->nom ?? 'Le nom a été supprimé'}} {{$absence->employe->prenom}}

                                </div>
                              </div>
                            <div class="modal-header">
                              <h4 class="modal-title">Rapport d'explication ou de justification</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <p>{{$absence->rapport_explication}}&hellip;</p>
                            </div>
                            <div class="modal-footer justify-content-between">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
                            </div>
                          </div>
                          <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                      </div>
                      <!-- /.modal -->

                  </td>
                  {{-- <td>
                      <div class="d-flex align-items-center list-user-action">

                          <form class="btn btn-sm bg-danger" data-toggle="tooltip" data-placement="top" title=""
                          data-original-title="Delete" method="POST" action="{{route('absences.destroy',$absence->id)}}">
                              @csrf
                              @method('DELETE')
                              <button type="submit" class=""><i class="mr-0 ri-delete-bin-line"></i>Justification</button>

                          </form>
                      </div>
                  </td> --}}
              </tr>
            @endif
          @endforeach


      </tfoot>
    </table>
  </div>
