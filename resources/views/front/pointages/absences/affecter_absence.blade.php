
            <div class="card card-info">
                <div class="card-header">
                <h3 class="card-title">Affecter une absence</h3>
                <span class="breadcrumb float-sm-right"><a href="{{route('absences.index')}}">Retour</a></span>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                     <!-- form start -->
              <form method="POST" enctype="multipart/form-data" action="{{route('absences.store')}}" id="form1">
                @csrf
                <div class="card-body">
                    <div class="row">
                        
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Date absence</label>
                                <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                    <input type="text" name="date_absence" id="reservationdate" class="form-control datetimepicker-input" data-target="#reservationdate"/>
                                    <div class="input-group-append" data-target="#reservationdate"  data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                              </div>
                        </div>
                 
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="">Employé(e):</label>
                                <select name="employe_id" data-placeholder="" class="form-control form-control-select2" data-fouc>
                                    <option aria-selected="seleted">Veuillez sélectionnez l'employé(e)</option>
                                    @foreach ($employes as $employe)
                                        <option value="{{$employe->id}}"> {{$employe->nom}}</option>
                                        
                                    @endforeach
                                </select>
                              </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Pointer absent</button>
                </div>
              </form>

                </div>
                <!-- /.card-body -->
            </div>