@extends('layouts.main')
@section('content')


  <!-- Navbar -->
    @include('layouts.header')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->

  @include('layouts.sidebars')

<div class="wrapper">



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">


    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">


        <div class="row">

          <div class="col-12 col-sm-12">
            <div class="card card-info card-tabs">
              <div class="p-0 pt-1 card-header">
                <ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">
                  <li class="px-3 pt-2"><h3 class="card-title">Cahier de pointage</h3></li>
                  @include('flash::message')
                  <li class="nav-item">
                    <a class="nav-link active" id="custom-tabs-two-home-tab" data-toggle="pill" href="#custom-tabs-two-home" role="tab" aria-controls="custom-tabs-two-home" aria-selected="true">Liste des présents</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-two-profile-tab" data-toggle="pill" href="#custom-tabs-two-profile" role="tab" aria-controls="custom-tabs-two-profile" aria-selected="false">Liste des absents</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-two-messages-tab" data-toggle="pill" href="#custom-tabs-two-messages" role="tab" aria-controls="custom-tabs-two-messages" aria-selected="false">Demisions</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-two-settings-tab" data-toggle="pill" href="#custom-tabs-two-settings" role="tab" aria-controls="custom-tabs-two-settings" aria-selected="false">Créer un pointage</a>
                  </li>
                </ul>
              </div>
              <div class="card-body">
                <div class="tab-content" id="custom-tabs-two-tabContent">
                  <div class="tab-pane fade show active" id="custom-tabs-two-home" role="tabpanel" aria-labelledby="custom-tabs-two-home-tab">
                     {{-- Presents --}}
                     <div class="card card-info">
                        <div class="card-header">
                        <h3 class="card-title">Liste des présents</h3>
                        {{-- <span class="breadcrumb float-sm-right"><a href="{{route('absences.create')}}">Marquer une présence</a></span> --}}
                        </div>
                            <!-- /.card-header -->
                            @include("front.pointages.absences.presents")
                            <!-- /.card-body -->
                    </div>
                  </div>
                  <div class="tab-pane fade" id="custom-tabs-two-profile" role="tabpanel" aria-labelledby="custom-tabs-two-profile-tab">
                     {{-- Absence --}}
                     <div class="card card-info">
                        <div class="card-header">
                        <h3 class="card-title">Liste des absents</h3>
                        {{-- <span class="breadcrumb float-sm-right"><a href="{{route('absences.create')}}">Affecter une absence</a></span> --}}
                        </div>
                            <!-- /.card-header -->
                            @include("front.pointages.absences.absents")
                            <!-- /.card-body -->
                    </div>
                  </div>
                  <div class="tab-pane fade" id="custom-tabs-two-messages" role="tabpanel" aria-labelledby="custom-tabs-two-messages-tab">
                     {{-- Demission --}}
                     <div class="card card-info">
                        <div class="card-header">
                        <h3 class="card-title">Demissions</h3>
                        {{-- <span class="breadcrumb float-sm-right"><a href="{{route('absences.create')}}">Demande de demission</a></span> --}}
                        </div>
                            <!-- /.card-header -->
                            @include("front.pointages.absences.demissions")
                            <!-- /.card-body -->
                    </div>
                  </div>
                  <div class="tab-pane fade" id="custom-tabs-two-settings" role="tabpanel" aria-labelledby="custom-tabs-two-settings-tab">
                    @include("front.pointages.absences.create")

                    {{-- <select name="employe_id" data-placeholder="Select employé" class="form-control form-control-select2 required" data-fouc>
                        <option selected="selected">Selectionnez un employé</option>
                                @foreach ($employes as $employe)
                                <option value="{{$employe->id}}">{{$employe->nom}} {{$employe->prenom}}</option>
                                @endforeach
                    </select> --}}
                  </div>
                </div>
              </div>
              <!-- /.card -->
            </div>
          </div>
        </div>



        <!-- /.card -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@include('layouts.footer')

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->


@endsection

