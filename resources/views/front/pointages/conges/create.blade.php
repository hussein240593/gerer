
            <div class="card card-info">
                <div class="card-header">
                    <h3 class="card-title">Demande de congé</h3>
                    {{-- <span class="breadcrumb float-sm-right"><a href="{{route('conges.index')}}">Retour</a></span> --}}
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <strong>Tous les champs </strong><span class="text-danger">("*")</span><strong> sont obligatoires</strong>
                    <hr>
                     <!-- form start -->
                     @include('flash::message')
                    <form method="POST" enctype="multipart/form-data" action="{{route('conges.store')}}" id="form1">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="Employé(e)">{{ __('Employé(e)') }} <span class="text-danger">*</span></label>
                                        <select class="form-control @error('employe_id') is-invalid @enderror" name="employe_id" value="{{ old('employe_id') }}" required autocomplete="employe_id" autofocus>
                                            <option value="-1" selected="selected">Selectionnez un employé</option>
                                                    @foreach ($employes as $employe)
                                                    <option value="{{$employe->id}}">{{$employe->nom}} {{$employe->prenom}}</option>
                                                    @endforeach
                                        </select>
                                        @error('employe_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="Date debut">{{ __('Date debut') }} <span class="text-danger">*</span></label>
                                        <div class="input-group date" id="reservationdatetime" data-target-input="nearest">
                                            <input type="text" class="form-control datetimepicker-input @error('date_debut_conge') is-invalid @enderror" name="date_debut_conge" value="{{ old('date_debut_conge') }}" required autocomplete="date_debut_conge" autofocus data-target="#reservationdatetime"/>
                                            @error('date_debut_conge')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                            <div class="input-group-append" data-target="#reservationdatetime" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="Date fin">{{ __('Date fin') }} <span class="text-danger">*</span></label>
                                        <div class="input-group date" id="reservationdatetime2" data-target-input="nearest">
                                            <input type="text" name="date_fin" class="form-control datetimepicker-input @error('date_fin') is-invalid @enderror" name="date_fin" value="{{ old('date_fin') }}" required autocomplete="date_fin" autofocus data-target="#reservationdatetime2"/>
                                            @error('date_fin')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                            <div class="input-group-append" data-target="#reservationdatetime2" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>


                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="Motif congé">{{ __('Motif congé') }} <span class="text-danger">*</span></label>
                                        <textarea rows="5" class="form-control @error('motif_conge') is-invalid @enderror" name="motif_conge" value="{{ old('motif_conge') }}" required autocomplete="motif_conge" autofocus></textarea>
                                        @error('motif_conge')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                {{-- <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nombre de jours</label>
                                        <input type="text"name="nombre_jours_conge" class="form-control" id="nmbreJour" readonly>
                                    </div>
                                </div> --}}
                            </div>

                            <!-- /.card-body -->

                            <div class="card-footer" style="text-align:center">
                            <button type="submit" class="btn btn-warning">Enregistrer le congé</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
                <!-- /.card-body -->
