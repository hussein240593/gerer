<!-- /.card-header -->
                    <div class="card-body">
                      <table id="example1" class="table table-bordered table-striped">
                          <thead>
                          <tr>
                              <th>Nom et prenoms</th>
                              {{-- <th>Nombre de jours</th> --}}
                              <th>Motif</th>
                              <th>Date début</th>
                              <th>Date fin</th>
                              <th>Lettre d'autorisation</th>
                              <th class="text-center">Status</th>

                          </tr>
                          </thead>
                          <tbody>

                              @foreach ($listeCongesEnAttente as $CongesEnAttente )
                              <tr>

                                  <td>
                                      @php
                                          $employes = App\Models\Employe::where('id',$CongesEnAttente->employe_id)->find($CongesEnAttente->employe_id);
                                          if($employes===null){
                                              echo 'Le nom a été supprimé';
                                          }else{
                                              echo "$employes->nom $employes->prenom";
                                          }
                                      @endphp
                                  </td>
                                  {{-- <td>
                                      @php
                                          $date1 = date('Y-m-d');
                                          $date2 = $CongesEnAttente->date_fin;
                                          $date = strtotime($date2) - strtotime( $date1);
                                          $nmbrJ = $date/86400;
                                                if($nmbrJ>=0){

                                                  echo "<span class='badge badge-success'>".$nmbrJ."</span> ";

                                                      } elseif ($nmbrJ<0) {
                                                      echo "<span class='badge badge-danger'>"."0"."</span> ";
                                                  }
                                      @endphp
                                  </td> --}}

                                  <td>

                                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-sm{{$CongesEnAttente->slug}}">
                                        <i class="far fa-eye"></i>
                                    </button>
                                     {{-- MODAL2 Absence justifié --}}
                                    <div class="modal fade" id="modal-sm{{$CongesEnAttente->slug}}">
                                        <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="ribbon-wrapper ribbon-lg">
                                                <div class="ribbon bg-info">
                                                    {{$CongesEnAttente->employe->nom}}

                                                </div>
                                            </div>
                                            <div class="modal-header">
                                            <h4 class="modal-title">Motif du congé</h4>
                                            {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">
                                                    {{$CongesEnAttente->motif}}
                                                </span>
                                            </button> --}}
                                            </div>
                                            <div class="modal-body">
                                            <p>{{$CongesEnAttente->motif_conge}}</p>
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Sortir</button>
                                            {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    <!-- /.modal -->
                                </td>
                                  <td>{{$CongesEnAttente->date_debut_conge}}</td>
                                  {{-- <td>{{$CongesEnAttente->date_fin}}</td> --}}
                                  <td>{{date('d/m/Y',strtotime($CongesEnAttente->date_fin))}}</td>
                                  <td>
                                    @if($CongesEnAttente->lettre_autorisation==NULL)
                                        <button type="button" class="btn"style="background-color: hsla(226, 92%, 15%, 0.911) !important;color:white" data-toggle="modal" data-target="#modal-lg2{{$CongesEnAttente->slug}}">
                                            Rediger l'autorisation
                                        </button>
                                    @else
                                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-lg4{{$CongesEnAttente->slug}}">
                                        <i class="far fa-eye"></i>
                                    </button>
                                    @endif

                                        {{-- MODAL2 writting lt autorisation --}}
                                        <div class="modal fade" id="modal-lg2{{$CongesEnAttente->slug}}">
                                            <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                <h4 class="modal-title">
                                                    {{-- @php
                                                        $employes = App\Models\Employe::where('id',$absence->employe_id)->find($absence->employe_id);
                                                        if($employes===null){
                                                            echo 'Le nom a été supprimé';
                                                        }else{
                                                            echo "$employes->nom $employes->prenom "."veuillez vous justifier";
                                                        }
                                                    @endphp --}}
                                                </h4>

                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>


                                                </div>
                                                <div class="modal-body">
                                                    <p>
                                                        <form method="POST" enctype="multipart/form-data" class="wizard-form" action="{{route('autorisations',$CongesEnAttente->id)}}">
                                                            @csrf
                                                            {{-- @method('put') --}}
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <div class="form-group">
                                                                        <label for="">Rediger la lettre d'autorisation</label>
                                                                        <textarea required name="lettre_autorisation" class="form-control" style="height: 300px" id="summernote"></textarea>
                                                                    </div>
                                                                </div>

                                                                {{-- <div class="col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="exampleInputEmail1">Selectionner</label>
                                                                        <input type="file" name="rapport_explication" class="form-control" id="mobno" placeholder="Remplissez cette fiche de demande d'explication pour justifier votre absence du {{$absence->date_absence}} ">
                                                                    </div>
                                                                </div> --}}
                                                            </div>

                                                            <div class="modal-footer justify-content-between">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">fermer</button>
                                                                <button type="submit" class="btn btn-primary">Justifier</button>
                                                            </div>
                                                        </form>
                                                    </p>
                                                </div>

                                            </div>
                                            <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                        <!-- /.modal -->

                                        {{-- MODAL4 read lt autorisation --}}
                                        <div class="modal fade" id="modal-lg4{{$CongesEnAttente->slug}}">
                                            <div class="modal-dialog modal-lg">
                                              <div class="modal-content">
                                                <div class="ribbon-wrapper ribbon-lg">
                                                    <div class="ribbon bg-info">
                                                        {{$CongesEnAttente->employe->nom ?? 'Le nom a été supprimé'}} {{$CongesEnAttente->employe->prenom}}

                                                    </div>
                                                  </div>
                                                <div class="modal-header">
                                                  <h4 class="modal-title">Lettre d'autorisation</h4>
                                                  {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                  </button> --}}
                                                </div>
                                                <div class="modal-body">
                                                  <p>{{$CongesEnAttente->lettre_autorisation}}&hellip;</p>
                                                </div>
                                                <div class="modal-footer justify-content-between">
                                                  <button type="button" class="btn btn-default" data-dismiss="modal">Sortir</button>
                                                  {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
                                                </div>
                                              </div>
                                              <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                        <!-- /.modal -->
                                  </td>

                                  <td>
                                      @if($CongesEnAttente->is_motif_refus ==2)
                                          <div class="d-flex align-items-center list-user-action">
                                              <span>Non autorisé </span>
                                          <a class="badge badge-warning" data-toggle="tooltip" data-placement="top" title=""
                                              data-original-title="Relancé" href="{{route('congeValide',$CongesEnAttente->id)}}">
                                              Relancer
                                              {{-- <i class="mr-0 ri-user-add-line"></i> --}}
                                          </a>
                                          @elseif($CongesEnAttente->is_motif_refus ==1)

                                          <span class="badge badge-success">Autorisé</span>
                                          @else
                                          {{-- <a class="btn btn-sm bg-primary" data-toggle="tooltip" data-placement="top" title=""
                                              data-original-title="Delete" href="#">
                                              <i class="mr-0 ri-delete-bin-line"></i>
                                              </a> --}}
                                              <a class="badge badge-primary" data-toggle="tooltip" data-placement="top" title=""
                                                  data-original-title="Accepté" href="{{route('congeValide',$CongesEnAttente->id)}}">
                                                  Accepter
                                                  {{-- <i class="mr-0 ri-pencil-line"></i> --}}
                                              </a>
                                              <a href="{{route('congeRefuse',$CongesEnAttente->id)}}" class="badge badge-danger" data-toggle="tooltip" data-placement="top" title=""
                                              data-original-title="Refusé">
                                                  Refuser
                                                  {{-- <i class="mr-0 ri-delete-bin-line"></i> --}}
                                              </a>

                                          </div>
                                      @endif
                                  </td>
                              </tr>
                          @endforeach


                          </tfoot>
                      </table>
                    </div>
                    <!-- /.card-body -->
