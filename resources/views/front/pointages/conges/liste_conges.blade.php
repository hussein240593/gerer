<!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                @include('sweetalert::alert')
                @include('flash::message')
                <thead>
                <tr>
                    <th>Nom et prenoms</th>
                    <th>Nombre de jours</th>
                    <th>Motif</th>
                    <th>Lettre autorisation</th>
                    <th>Date debut</th>
                    <th>Date fin</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Etat</th>
                    <th class="text-center">Action</th>

                </tr>
                </thead>
                <tbody>
                    @foreach ($conges as $conge )
                    <tr>
                        {{-- <td class="text-center"><img class="rounded img-fluid avatar-40"src="../assets/images/user/01.jpg" alt="profile"></td> --}}
                        <td>{{$conge->employe->nom}} {{$conge->employe->prenom}}</td>


                        <td>
                            @php
                                $date1 = date('Y-m-d');
                                $date2 = $conge->date_fin;
                                $date = strtotime($date2) - strtotime( $date1);
                                $nmbrJ = $date/86400;
                                      if($nmbrJ>=0){

                                        echo "<span class='badge badge-success'>".$nmbrJ."</span> ";

                                            } elseif ($nmbrJ<0) {
                                            echo "<span class='badge badge-danger'>"."0"."</span> ";
                                        }
                            @endphp
                            {{-- @php

                                $date1 = $conge->date_debut_conge;
                                $date2 = $conge->date_fin_conge;

                                $date = (strtotime(( $date2)) - (strtotime( $date1)));
                                $nmbrJ = $date/86400;
                                echo $nmbrJ;


                            @endphp --}}
                        </td>
                        <td>

                            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-lg1{{$conge->slug}}">
                                <i class="far fa-eye"></i>
                             </button>
                              {{-- MODAL2 Absence justifié --}}
                             <div class="modal fade" id="modal-lg1{{$conge->slug}}">
                                 <div class="modal-dialog modal-lg">
                                 <div class="modal-content">
                                     <div class="ribbon-wrapper ribbon-lg">
                                         <div class="ribbon bg-info">
                                             {{$conge->employe->nom}}

                                         </div>
                                     </div>
                                     <div class="modal-header">
                                     <h4 class="modal-title">Motif du congé</h4>

                                     </div>
                                     <div class="modal-body">
                                     <p>{{$conge->motif_conge}}</p>
                                     </div>
                                     <div class="modal-footer justify-content-between">
                                     <button type="button" class="btn btn-default" data-dismiss="modal">Sortir</button>
                                     {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
                                     </div>
                                 </div>
                                 <!-- /.modal-content -->
                                 </div>
                                 <!-- /.modal-dialog -->
                             </div>
                             <!-- /.modal -->
                        </td>
                        <td>
                            @if($conge->lettre_autorisation !=NULL)
                            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-lg3{{$conge->slug}}">
                                <i class="far fa-eye"></i>
                            </button>
                            @else
                            <span>Non redigé</span>
                            @endif
                            {{-- MODAL3 read lt autorisation --}}
                            <div class="modal fade" id="modal-lg3{{$conge->slug}}">
                                <div class="modal-dialog modal-lg">
                                  <div class="modal-content">
                                    <div class="ribbon-wrapper ribbon-lg">
                                        <div class="ribbon bg-info">
                                            {{$conge->employe->nom ?? 'Le nom a été supprimé'}} {{$conge->employe->prenom}}

                                        </div>
                                      </div>
                                    <div class="modal-header">
                                      <h4 class="modal-title">Lettre d'autorisation</h4>
                                      {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button> --}}
                                    </div>
                                    <div class="modal-body">
                                      <p>{{$conge->lettre_autorisation}}&hellip;</p>
                                    </div>
                                    <div class="modal-footer justify-content-between">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Sortir</button>
                                      {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
                                    </div>
                                  </div>
                                  <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->


                        </td>
                        <td>{{ \Carbon\Carbon::parse(($conge->date_debut_conge))->isoFormat('LLL') }}</td>
                        {{-- <td>{{date('d/m/Y',strtotime($conge->date_fin))}}</td> --}}
                        <td>{{ \Carbon\Carbon::parse(($conge->date_fin))->isoFormat('LLL') }}</td>

                        <td>
                            @php
                                // echo \Carbon\Carbon::parse('2019-07-23 14:51')->locale('fr_FR')->isoFormat('LLLL');
                                $date1 = date('Y-m-d');
                                $date2 = $conge->date_fin;
                                $date = strtotime($date2) - strtotime( $date1);
                                $nmbrJ = $date/86400;
                                      if($nmbrJ>=0 && $conge->is_motif_refus ==1){

                                        echo "<span class='badge badge-success'>"."En congé"."</span> ";


                                            // } elseif ($nmbrJ<0) {
                                            // echo "<span class='badge badge-danger'>Fin congé</span> ";
                                      }elseif ($nmbrJ>=0 && $conge->is_motif_refus ==0) {
                                        echo "<span class='badge badge-white'>"."Pas en congé"."</span> ";
                                      }elseif ($nmbrJ<0) {
                                        echo "<span class='badge badge-default'>Fin congé</span> ";
                                      }elseif ($nmbrJ>=0 && $conge->is_motif_refus ==2) {
                                        echo "<span class='badge badge-danger'>Congé refusé</span> ";
                                      }
                            @endphp
                        </td>


                        <td>
                            @if($conge->is_motif_refus ==0)
                                <div class="d-flex align-items-center list-user-action">
                                <span class="badge badge-warning">En attente</span>
                                @elseif($conge->is_motif_refus ==1)
                                    <span class="btn-iframe-close"style="color:#02e74e;">
                                        <i class="fas fa-check"></i>
                                    </span>
                                @else

                                    <span class=""style="color:#dc3545">
                                    <i class="fas fa-times"></i>
                                    </span>
                                </div>
                            @endif
                         </td>
                         <td>
                            <div class="d-flex align-items-center list-user-action">

                                <form class="" data-toggle="tooltip" data-placement="top" title=""
                                data-original-title="Delete" method="POST" action="{{route('conges.destroy',$conge->id)}}">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class=""><i class="far fa-trash-alt"></i></button>

                                </form>
                            </div>
                         </td>
                     </tr>
                @endforeach


                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
