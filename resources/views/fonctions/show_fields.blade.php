<!-- Id Type Fonction Field -->
<div class="col-sm-12">
    {!! Form::label('id_type_fonction', 'Id Type Fonction:') !!}
    <p>{{ $fonction->id_type_fonction }}</p>
</div>

<!-- Libelle Field -->
<div class="col-sm-12">
    {!! Form::label('libelle', 'Libelle:') !!}
    <p>{{ $fonction->libelle }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $fonction->description }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $fonction->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $fonction->updated_at }}</p>
</div>

