<div class="table-responsive">
    <table class="table" id="fonctions-table">
        <thead>
        <tr>
            <th>Id Type Fonction</th>
        <th>Libelle</th>
        <th>Description</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($fonctions as $fonction)
            <tr>
                <td>
                    @php
                    $type_fonctions = App\Models\Type_fonction::where('id',$fonction->id_type_fonction)->find($fonction->id_type_fonction);
                    if($type_fonctions===null){
                        echo 'Type fonction a été supprimé';
                    }else{
                        echo $type_fonctions->libelle;
                    }
                @endphp
                </td>
            <td>{{ $fonction->libelle }}</td>
            <td>{{ $fonction->description }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['fonctions.destroy', $fonction->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('fonctions.show', [$fonction->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('fonctions.edit', [$fonction->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
