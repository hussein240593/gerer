<!-- Id Type Fonction Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_type_fonction', 'Id Type Fonction:') !!}
    {!! Form::select('id_type_fonction',$type_fonctions->pluck('libelle', 'id'), null, ['placeholder' => 'Choisir un type fonction...']) !!}
</div>


<!-- Libelle Field -->
<div class="form-group col-sm-6">
    {!! Form::label('libelle', 'Libelle:') !!}
    {!! Form::text('libelle', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>
