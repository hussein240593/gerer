<!-- Nom Field -->
<div class="col-sm-12">
    {!! Form::label('nom', 'Nom:') !!}
    <p>{{ $departement->nom }}</p>
</div>

<!-- Adr Depart Field -->
<div class="col-sm-12">
    {!! Form::label('adr_depart', 'Adr Depart:') !!}
    <p>{{ $departement->adr_depart }}</p>
</div>

<!-- Ville Depart Field -->
<div class="col-sm-12">
    {!! Form::label('ville_depart', 'Ville Depart:') !!}
    <p>{{ $departement->ville_depart }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $departement->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $departement->updated_at }}</p>
</div>

