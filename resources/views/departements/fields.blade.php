<!-- Nom Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nom', 'Nom:') !!}
    {!! Form::text('nom', null, ['class' => 'form-control']) !!}
</div>

<!-- Adr Depart Field -->
<div class="form-group col-sm-6">
    {!! Form::label('adr_depart', 'Adr Depart:') !!}
    {!! Form::text('adr_depart', null, ['class' => 'form-control']) !!}
</div>

<!-- Ville Depart Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ville_depart', 'Ville Depart:') !!}
    {!! Form::text('ville_depart', null, ['class' => 'form-control']) !!}
</div>