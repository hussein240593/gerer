<div class="table-responsive">
    <table class="table" id="departements-table">
        <thead>
        <tr>
            <th>Nom</th>
        <th>Adr Depart</th>
        <th>Ville Depart</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($departements as $departement)
            <tr>
                <td>{{ $departement->nom }}</td>
            <td>{{ $departement->adr_depart }}</td>
            <td>{{ $departement->ville_depart }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['departements.destroy', $departement->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('departements.show', [$departement->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('departements.edit', [$departement->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
