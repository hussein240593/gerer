<?php

namespace App\Http\Controllers;


use App\Models\Employe;
use App\Models\Fonction;
use App\Models\Absence;
use App\Models\Type_fonction;
use illuminate\Support\Str;
use App\Models\Departement;
use App\Models\Dossier_employe;
use App\Models\Ville;
use Flash;
// use App\Models\rapportPersonne;
use Illuminate\Http\Request;

class EmployeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {


        // $employe = Absence::find(1);

        // return dd($employe->absence->date_absence);
        $absences = Absence::get();


        $employes = Employe::get();
        $dossierEmployes = Dossier_employe::get();
              // recherche
              if($request->has('search')){
                $dossierEmployes = Dossier_employe::where('cv','like',"%{$request->search}%")->orWhere('lettre_motivation','like',"%{$request->search}%")->get();
// dd($dossierEmployes);
            }

        $departements = Departement::all();
        $fonctions = Fonction::all();
        $types_fonctions = Type_fonction::all();
        $villes = Ville::all();
        return view('front.employes.index',compact('employes','absences','departements','fonctions','types_fonctions','villes','dossierEmployes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        $departements = Departement::all();
        $fonctions = Fonction::all();
        $types_fonctions = Type_fonction::all();
        return view('front.employes.create',compact('departements','types_fonctions','fonctions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->departement_id<>-1){
            $employes= new Employe();
            $employes->nom = $request->nom;
            $employes->prenom = $request->prenom;
            $employes->adr_postal = $request->prenom;
            $employes->sexe = $request->sexe;
            $employes->email = $request->email;
            $employes->departement_id = $request->departement_id;
            $employes->ville_id = $request->ville_id;

            $employes->telephone = $request->telephone;
            $employes->type_fonction_id = $request->type_fonction_id;
            $employes->fonction_id = $request->fonction_id;
            $employes->birthday = $request->birthday;


            // dd($employes);
            $employes->save();

        }else{
            return back();
        }





        // $dossier ->personne_id = $personnes->id;
        // $dossier-> cv = $request->cv;
        // $dossier->lettre_motivation = $request->lettre_motivation;
        // $dossier->type_piece = $request->type_piece;
        // $dossier->numero_piece = $request->numero_piece;
        // $dossier->save();
        // return redirect()->back()->with('success', 'L\'enregistrement a été effectué avec succès');
        // toastr()->success('Insertion performed successfully', __('Success'));
        return redirect()->route('employes.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employe  $employe
     * @return \Illuminate\Http\Response
     */
    public function show(string $id)
    {
        $employe = Employe::where('id', $id)->firstOrFail();

        $dossierEmployes = Dossier_employe::where('id', $id)->firstOrFail();
        return view('front.employes.show',compact('employe','dossierEmployes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employe  $employe
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employe = Employe::where('id', $id)->firstOrFail();
        $departements = Departement::all();
        $types_fonctions = Type_fonction::all();
        $fonctions = Fonction::all();

        return view('front.employes.edit',compact('employe','departements','types_fonctions','fonctions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employe  $employe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employe $employe)
    {
        $employe->update(request()->all());

        return redirect()->route('employes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employe  $employe
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employe $employe)
    {
        $employe->delete();
        return redirect()->route('employes.index');
    }


    // DOSSIER EMPLOYES

    public function Dossier(Request $request){


        if($request->avatar!=""){
                // Avatars
                $photo = $input['avatar'] = time().'.'.$request->avatar->getClientOriginalExtension();

                $request->avatar->move(public_path('images/avatars'), $photo);
                // Insertion du lt
                $lt = $input['lettre_motivation'] = time().'.'.$request->lettre_motivation->getClientOriginalExtension();

                $request->lettre_motivation->move(public_path('pdf'), $lt);

                $dip = $input['diplome'] = time().'.'.$request->diplome->getClientOriginalExtension();

                $request->diplome->move(public_path('pdf'), $dip);

            // Insertion du cv
            $img = $input['cv'] = time().'.'.$request->cv->getClientOriginalExtension();
            $request->cv->move(public_path('pdf'), $img);


            $dossierEmploye= new Dossier_employe();
            // Dossier personne
            $dossierEmploye->avatar = $input['avatar']= $photo;
            $dossierEmploye->cv = $input['cv']=$img;
            $dossierEmploye->diplome = $input['diplome']=$dip;

            $dossierEmploye->lettre_motivation = $input['lettre_motivation']=$lt;
            $dossierEmploye->type_piece = $request->type_piece;
            $dossierEmploye->number_of_piece = $request->number_of_piece;
            $dossierEmploye->employe_id = $request->employe_id;
            // dd($dossierEmploye);s
            $dossierEmploye->save();
            //
            $slug = Str::of($dossierEmploye->number_of_piece."60c1e96d9d064c36205c4097".$dossierEmploye->id_employe.$dossierEmploye->created_at)->slug('-');
            $dossierEmploye->slug =  $slug;

            $dossierEmploye->save();

    }else{
        $dossierEmploye= new Dossier_employe();


        // Dossier personne
        $dossierEmploye->type_piece = $request->type_piece;
        $dossierEmploye->number_of_piece = $request->number_of_piece;
        $dossierEmploye->employe_id = $request->employe_id;
        $dossierEmploye->save();
        //
        $slug = Str::of($dossierEmploye->number_of_piece."60c1e96d9d064c36205c4097".$dossierEmploye->id_employe.$dossierEmploye->created_at)->slug('-');
        $dossierEmploye->slug =  $slug;

        $dossierEmploye->save();
    }

        Flash::success('Dossier Employe enregistré avec succès.');

        return redirect(route('employes.index'));
    }

    public function Demission(Request $request, $id){

        $demissions = Employe::where('id',$id)->find($id);

        $demissions->update(request()->all());

        // dd($demissions);
        Flash::success('Dossier Employe enregistré avec succès.');
        return redirect(route('employes.index'));
    }



}



