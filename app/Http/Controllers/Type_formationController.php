<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateType_formationRequest;
use App\Http\Requests\UpdateType_formationRequest;
use App\Repositories\Type_formationRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class Type_formationController extends AppBaseController
{
    /** @var  Type_formationRepository */
    private $typeFormationRepository;

    public function __construct(Type_formationRepository $typeFormationRepo)
    {
        $this->typeFormationRepository = $typeFormationRepo;
    }

    /**
     * Display a listing of the Type_formation.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $typeFormations = $this->typeFormationRepository->all();

        return view('type_formations.index')
            ->with('typeFormations', $typeFormations);
    }

    /**
     * Show the form for creating a new Type_formation.
     *
     * @return Response
     */
    public function create()
    {
        return view('type_formations.create');
    }

    /**
     * Store a newly created Type_formation in storage.
     *
     * @param CreateType_formationRequest $request
     *
     * @return Response
     */
    public function store(CreateType_formationRequest $request)
    {
        $input = $request->all();

        $typeFormation = $this->typeFormationRepository->create($input);

        Flash::success('Type Formation saved successfully.');

        return redirect(route('typeFormations.index'));
    }

    /**
     * Display the specified Type_formation.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $typeFormation = $this->typeFormationRepository->find($id);

        if (empty($typeFormation)) {
            Flash::error('Type Formation not found');

            return redirect(route('typeFormations.index'));
        }

        return view('type_formations.show')->with('typeFormation', $typeFormation);
    }

    /**
     * Show the form for editing the specified Type_formation.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $typeFormation = $this->typeFormationRepository->find($id);

        if (empty($typeFormation)) {
            Flash::error('Type Formation not found');

            return redirect(route('typeFormations.index'));
        }

        return view('type_formations.edit')->with('typeFormation', $typeFormation);
    }

    /**
     * Update the specified Type_formation in storage.
     *
     * @param int $id
     * @param UpdateType_formationRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateType_formationRequest $request)
    {
        $typeFormation = $this->typeFormationRepository->find($id);

        if (empty($typeFormation)) {
            Flash::error('Type Formation not found');

            return redirect(route('typeFormations.index'));
        }

        $typeFormation = $this->typeFormationRepository->update($request->all(), $id);

        Flash::success('Type Formation updated successfully.');

        return redirect(route('typeFormations.index'));
    }

    /**
     * Remove the specified Type_formation from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $typeFormation = $this->typeFormationRepository->find($id);

        if (empty($typeFormation)) {
            Flash::error('Type Formation not found');

            return redirect(route('typeFormations.index'));
        }

        $this->typeFormationRepository->delete($id);

        Flash::success('Type Formation deleted successfully.');

        return redirect(route('typeFormations.index'));
    }
}
