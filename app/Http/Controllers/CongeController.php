<?php

namespace App\Http\Controllers;
use illuminate\Support\Str;
use App\Models\Conge;
use App\Models\Employe;
use Illuminate\Http\Request;
use Flash;
use Alert;

class CongeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $conges = Conge::all();
        // Congé non valide
        $listeCongesEnAttente = Conge::all();

        $employes = Employe::all();// recuperer l'employe dans le create congé
        return view('front.pointages.conges.index',compact('conges','listeCongesEnAttente','employes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        // $employes = Employe::all();
        // return view('front.pointages.conges.create',compact('employes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->employe_id<>-1){
            $conges= new Conge();
            $conges->employe_id = $request->employe_id;
            $conges->date_debut_conge = $request->date_debut_conge;
            $conges->date_fin = $request->date_fin;
            $conges->motif_conge = $request->motif_conge;
            $conges->nombre_jours_conge = $request->nombre_jours_conge;
            if($request->date_fin<=$request->date_debut_conge){
                Flash::error('La date de debut ne doit pas etre superieur ou egale a la date de fin');
                return back();
            }
            $conges->save();

            $slug = Str::of($conges->date_debut."60c1e96d9d064c36205c4097".$conges->date_fin.$conges->created_at)->slug('-');
            $conges->slug =  $slug;

            $conges->save();
        }else{
            Flash::error('Veuillez selectionner un(e) employé(e)');
            return back();
        }
        alert()->success('Demande de congé effectuée avec succès');
        return redirect()->route('conges.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Conge  $conge
     * @return \Illuminate\Http\Response
     */
    public function show(Conge $conge)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Conge  $conge
     * @return \Illuminate\Http\Response
     */
    public function edit(Conge $conge)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Conge  $conge
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Conge $conge)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Conge  $conge
     * @return \Illuminate\Http\Response
     */
    public function destroy(Conge  $conge)
    {
        $conge->delete();
        return redirect()->back();
    }

    // LISTES DES CONGES EN COURS

    // public function listeCongesEnAttente()
    // {
    //     $listeCongesEnAttente = Conge::all();
    //     return view('front.pointages.conges.conge_attente',compact('listeCongesEnAttente'));
    // }


    public function Accepte(Request $request, $id){
        $accept_or_refus = Conge::where('id',$id)->find($id);

        $accept_or_refus->is_motif_refus=1;
        // dd($accept_or_refus);
        $accept_or_refus->update();

        return redirect()->route('conges.index');
    }
    public function Refuse(Request $request, $id){
        $accept_or_refus = Conge::where('id',$id)->find($id);
        $accept_or_refus->is_motif_refus=2;
        $accept_or_refus->update();

        return redirect()->route('conges.index');
    }

    // LETTRE D'AUTORISATION

    public function Autorisation(Request $request, $id){
        $lettre_autorisation = Conge::where('id',$id)->find($id);

        $lettre_autorisation->update(request()->all());
// dd($lettre_autorisation);
        return redirect()->route('conges.index');
    }



}
