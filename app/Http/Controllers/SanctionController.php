<?php

namespace App\Http\Controllers;

use App\Models\Sanction;
use Illuminate\Http\Request;
use App\Models\Employe;
use App\Models\Type_sanction;
use illuminate\Support\Str;

class SanctionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $sanctions = Sanction::get();
        $employes = Employe::all();
        $type_sanctions = Type_sanction::all();
        return view('front.sanctions.index',compact('sanctions','employes','type_sanctions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {      $type_sanctions = Type_sanction::all();
        $employes = Employe::all();
        return view('front.sanctions.create',compact('employes','type_sanctions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $sanctions= new Sanction();
        $sanctions->employe_id = $request->employe_id;
        $sanctions->date_debut = $request->date_debut;
        $sanctions->date_fin = $request->date_fin;
        $sanctions->type_sanction_id = $request->type_sanction_id;
        $sanctions->Date_de_comise_la_faute = $request->Date_de_comise_la_faute;
        $sanctions->motif = $request->motif;
        // $sanctions->nombre_jours_conge = $request->nombre_jours_conge;
        if($request->date_fin<=$request->date_debut){
            return back()->with('La date de debut ne doit pas etre superieur ou egale a la date de fin');
        }
        // dd($sanctions);
        $sanctions->save();

        $slug = Str::of($sanctions->date_debut."60c1e96d9d064c36205c4097".$sanctions->date_fin.$sanctions->created_at)->slug('-');
        $sanctions->slug =  $slug;

        $sanctions->save();

        return redirect()->route('sanctions.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sanction  $sanction
     * @return \Illuminate\Http\Response
     */
    public function show(Sanction $sanction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Sanction  $sanction
     * @return \Illuminate\Http\Response
     */
    public function edit(Sanction $sanction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Sanction  $sanction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sanction $sanction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sanction  $sanction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sanction $sanction)
    {
        //
    }

    public function decision(Request $request, $id)
    {
        dd($id);
        $sanction = Sanction::where('id',$id)->find($id);
        // $decisions = new Sanction();
        $sanction->update(request()->all());

        return redirect()->route('sanctions.index');
        // $decisions->decisionSanction = $request->decisionSanction;

    }


}
