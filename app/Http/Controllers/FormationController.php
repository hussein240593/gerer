<?php

namespace App\Http\Controllers;

use App\Models\Formation;
use App\Models\Type_formation;
use App\Models\Employe;
use Illuminate\Http\Request;
use illuminate\Support\Str;
class FormationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $liste_formations = Formation::get();

        return view('front.formations.index',compact('liste_formations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types_formations = Type_formation::get();
        $employes = Employe::get();
        return view('front.formations.create',compact('types_formations','employes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $formation = new Formation();
        $formation->libelle = $request->libelle;
        // $formation ->type_formation_id = $type_formation->id;
        // $formation ->personne_id = $personnes->id;
        $formation->date_debut = $request->date_debut;
        $formation->employe_id = $request->employe_id;
        $formation->type_formation_id = $request->type_formation_id;
        $formation->date_fin = $request->date_fin;
        $formation->number_of_day = $request->number_of_day;
        // dd($formation);
        $formation->save();

        $slug = Str::of($formation->libelle."60c1e96d9d064c36205c4097".$formation->libelle.$formation->created_at)->slug('-');
        $formation->slug =  $slug;
        // dd($personnes);
        $formation->save();

        // dd($formation);
        return redirect()->route('formations.index')->with('success', 'L\'enregistrement a été effectué avec succès');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Formation  $formation
     * @return \Illuminate\Http\Response
     */
    public function show(Formation $formation)
    {
        $formations = Formation::first();
        return view('front.formations.show',compact('formations'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Formation  $formation
     * @return \Illuminate\Http\Response
     */
    public function edit(Formation $formation)
    {
        $types_formations = Type_formation::all();
        $employes = Employe::all();
        return view('front.formations.edit',compact('types_formations','employes'),[
            'formation'=>$formation,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Formation  $formation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Formation $formation)
    {
        $formation->update(request()->all());
        return \redirect()->route('formations.index')->with('La mise à jour a été éffectué avec succès !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Formation  $formation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Formation $formation)
    {
        $formation->delete();
        return redirect()->back()->with('Suppression effectuées avec succès !');



        // $formation->delete();
        // return view('layouts.training.formation.index',compact('formation'));
    }
}

// CALCUL LE NOMBRE DE JOUR OU LA DUREE ENTRE DEUX DATE

// $date1 = $formation->date_debut;
// $date2 = $formation->date_fin;

// $date = (strtotime(( $date2)) - (strtotime( $date1)));
// $nmbrJ = $date/86400;
// echo $nmbrJ;


