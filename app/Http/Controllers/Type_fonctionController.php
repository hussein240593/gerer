<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateType_fonctionRequest;
use App\Http\Requests\UpdateType_fonctionRequest;
use App\Repositories\Type_fonctionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class Type_fonctionController extends AppBaseController
{
    /** @var  Type_fonctionRepository */
    private $typeFonctionRepository;

    public function __construct(Type_fonctionRepository $typeFonctionRepo)
    {
        $this->typeFonctionRepository = $typeFonctionRepo;
    }

    /**
     * Display a listing of the Type_fonction.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $typeFonctions = $this->typeFonctionRepository->all();

        return view('type_fonctions.index')
            ->with('typeFonctions', $typeFonctions);
    }

    /**
     * Show the form for creating a new Type_fonction.
     *
     * @return Response
     */
    public function create()
    {
        return view('type_fonctions.create');
    }

    /**
     * Store a newly created Type_fonction in storage.
     *
     * @param CreateType_fonctionRequest $request
     *
     * @return Response
     */
    public function store(CreateType_fonctionRequest $request)
    {
        $input = $request->all();

        $typeFonction = $this->typeFonctionRepository->create($input);

        Flash::success('Type Fonction saved successfully.');

        return redirect(route('typeFonctions.index'));
    }

    /**
     * Display the specified Type_fonction.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $typeFonction = $this->typeFonctionRepository->find($id);

        if (empty($typeFonction)) {
            Flash::error('Type Fonction not found');

            return redirect(route('typeFonctions.index'));
        }

        return view('type_fonctions.show')->with('typeFonction', $typeFonction);
    }

    /**
     * Show the form for editing the specified Type_fonction.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $typeFonction = $this->typeFonctionRepository->find($id);

        if (empty($typeFonction)) {
            Flash::error('Type Fonction not found');

            return redirect(route('typeFonctions.index'));
        }

        return view('type_fonctions.edit')->with('typeFonction', $typeFonction);
    }

    /**
     * Update the specified Type_fonction in storage.
     *
     * @param int $id
     * @param UpdateType_fonctionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateType_fonctionRequest $request)
    {
        $typeFonction = $this->typeFonctionRepository->find($id);

        if (empty($typeFonction)) {
            Flash::error('Type Fonction not found');

            return redirect(route('typeFonctions.index'));
        }

        $typeFonction = $this->typeFonctionRepository->update($request->all(), $id);

        Flash::success('Type Fonction updated successfully.');

        return redirect(route('typeFonctions.index'));
    }

    /**
     * Remove the specified Type_fonction from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $typeFonction = $this->typeFonctionRepository->find($id);

        if (empty($typeFonction)) {
            Flash::error('Type Fonction not found');

            return redirect(route('typeFonctions.index'));
        }

        $this->typeFonctionRepository->delete($id);

        Flash::success('Type Fonction deleted successfully.');

        return redirect(route('typeFonctions.index'));
    }
}
