<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDossier_employeRequest;
use App\Http\Requests\UpdateDossier_employeRequest;
use App\Repositories\Dossier_employeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Employe;
use Flash;
use Response;

class Dossier_employeController extends AppBaseController
{
    /** @var  Dossier_employeRepository */
    private $dossierEmployeRepository;

    public function __construct(Dossier_employeRepository $dossierEmployeRepo)
    {
        $this->dossierEmployeRepository = $dossierEmployeRepo;
    }

    /**
     * Display a listing of the Dossier_employe.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $dossierEmployes = $this->dossierEmployeRepository->all();
        $employes = Employe::all();
        return view('dossier_employes.index',compact('employes'))
            ->with('dossierEmployes', $dossierEmployes);
    }

    /**
     * Show the form for creating a new Dossier_employe.
     *
     * @return Response
     */
    public function create()
    {
        // return view('dossier_employes.create');
    }

    /**
     * Store a newly created Dossier_employe in storage.
     *
     * @param CreateDossier_employeRequest $request
     *
     * @return Response
     */
    public function store(CreateDossier_employeRequest $request)
    {
        dd($request);
        if($request->cv!=""){

            // Insertion du cv
            $img = $input['cv'] = time().'.'.$request->cv->getClientOriginalExtension();
            $request->cv->move(public_path('pdf'), $img);

                // Insertion du lt
            $lt = $input['lettre_motivation'] = time().'.'.$request->lettre_motivation->getClientOriginalExtension();

            $request->lettre_motivation->move(public_path('pdf'), $lt);

            $dip = $input['diplome'] = time().'.'.$request->diplome->getClientOriginalExtension();

            $request->diplome->move(public_path('pdf'), $dip);


            $dossierEmploye= new Dossier_employe();


            // Dossier personne
            $dossierEmploye->cv = $input['cv']=$img;
            $dossierEmploye->diplome = $input['diplome']=$dip;

            $dossierEmploye->lettre_motivation = $input['lettre_motivation']=$lt;
            $dossierEmploye->type_piece = $request->type_piece;
            $dossierEmploye->number_of_piece = $request->number_of_piece;
            $dossierEmploye->id_employe = $request->id_employe;

            $dossierEmploye->save();
            //
            $slug = Str::of($dossierEmploye->number_of_piece."60c1e96d9d064c36205c4097".$dossierEmploye->id_employe.$dossierEmploye->created_at)->slug('-');
            $dossierEmploye->slug =  $slug;
            //  dd($employes);
            $dossierEmploye->save();

    }else{
        $dossierEmploye= new Dossier_employe();


        // Dossier personne
        $dossierEmploye->type_piece = $request->type_piece;
        $dossierEmploye->number_of_piece = $request->number_of_piece;
        $dossierEmploye->id_employe = $request->id_employe;
        $dossierEmploye->save();
        //
        $slug = Str::of($dossierEmploye->number_of_piece."60c1e96d9d064c36205c4097".$dossierEmploye->id_employe.$dossierEmploye->created_at)->slug('-');
        $dossierEmploye->slug =  $slug;

        $dossierEmploye->save();
    }

        Flash::success('Dossier Employe enregistré avec succès.');

        return redirect(route('employes.index'));
    }

    /**
     * Display the specified Dossier_employe.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $dossierEmploye = $this->dossierEmployeRepository->find($id);

        if (empty($dossierEmploye)) {
            Flash::error('Dossier Employe not found');

            return redirect(route('dossierEmployes.index'));
        }

        return view('dossier_employes.show')->with('dossierEmploye', $dossierEmploye);
    }

    /**
     * Show the form for editing the specified Dossier_employe.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $dossierEmploye = $this->dossierEmployeRepository->find($id);

        if (empty($dossierEmploye)) {
            Flash::error('Dossier Employe not found');

            return redirect(route('dossierEmployes.index'));
        }

        return view('dossier_employes.edit')->with('dossierEmploye', $dossierEmploye);
    }

    /**
     * Update the specified Dossier_employe in storage.
     *
     * @param int $id
     * @param UpdateDossier_employeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDossier_employeRequest $request)
    {
        $dossierEmploye = $this->dossierEmployeRepository->find($id);

        if (empty($dossierEmploye)) {
            Flash::error('Dossier Employe not found');

            return redirect(route('dossierEmployes.index'));
        }

        $dossierEmploye = $this->dossierEmployeRepository->update($request->all(), $id);

        Flash::success('Dossier Employe updated successfully.');

        return redirect(route('dossierEmployes.index'));
    }

    /**
     * Remove the specified Dossier_employe from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $dossierEmploye = $this->dossierEmployeRepository->find($id);

        if (empty($dossierEmploye)) {
            Flash::error('Dossier Employe not found');

            return redirect(route('dossierEmployes.index'));
        }

        $this->dossierEmployeRepository->delete($id);

        Flash::success('Dossier Employe deleted successfully.');

        return redirect(route('dossierEmployes.index'));
    }
}
