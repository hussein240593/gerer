<?php

namespace App\Http\Controllers;
use illuminate\Support\Str;
use App\Models\Absence;
use App\Models\Rapport_justify_absence;
use App\Models\Employe;
use Illuminate\Http\Request;
use Flash;

class AbsenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employes = Employe::all();
        $absences = Absence::all();
        // dd( $absences);
        return view('front.pointages.absences.index',compact('absences','employes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        // $employes = Employe::all();
        // return view('front.pointages.absences.create',compact('employes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->is_status_present<>-1){
             // dd($request);
        $absences= new Absence();
        $absences->employe_id = $request->employe_id;
        $absences->date_absence = $request->date_absence;

        $absences->is_status_present = $request->is_status_present;
        // $absences->date_fin = $request->date_fin;
        $absences->save();

        }else{
            return back();
        }


        $slug = Str::of($absences->date_absence."husseinEstUnDeveloppeur".$absences->date_absence.$absences->created_at)->slug('-');
        $absences->slug =  $slug;

        $absences->save();

        Flash::success('Pointage éffectué avec succès.');
        return redirect()->route('absences.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Absence  $absence
     * @return \Illuminate\Http\Response
     */
    public function show(Absence $absence)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Absence  $absence
     * @return \Illuminate\Http\Response
     */
    public function edit(Absence $absence)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Absence  $absence
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Absence $absence)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Absence  $absence
     * @return \Illuminate\Http\Response
     */
    public function destroy(Absence $absence)
    {
        $absence->delete();

        return redirect()->route('absences.index');
    }
    
    // RAPPORT D'EXPLICATION OU DE DEMISSION
    public function justify(Request $request, $id){

        $absence = Absence::where('id',$id)->find($id);

        $absence->update(request()->all());

        return redirect()->route('absences.index');
    }

    // Accept demission status
    public function Etat_accepte_demission(Request $request, $id){
        $is_accept= Absence::where('id',$id)->find($id);
        $is_accept->is_etat_accepte_demission=1;
        //Recuperation des informations de l'employee en fonction de l'id Absence
        // $idemp = $is_accept->employe_id;
        // $is_employe= Employe::where('id',$idemp)->find($idemp);
        // dd($is_employe);
        $is_accept->update();


        return redirect()->route('absences.index');
    }
// Refuse demission status
    public function Etat_refus_demission(Request $request, $id){
        $is_refus= Absence::where('id',$id)->find($id);
        $is_refus->is_etat_accepte_demission=2;
        // dd($is_refus);
        $is_refus->update();


        return redirect()->route('absences.index');
    }
}
