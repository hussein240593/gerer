<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateFonctionRequest;
use App\Http\Requests\UpdateFonctionRequest;
use App\Repositories\FonctionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Type_fonction;
use Flash;
use Response;

class FonctionController extends AppBaseController
{
    /** @var  FonctionRepository */
    private $fonctionRepository;

    public function __construct(FonctionRepository $fonctionRepo)
    {
        $this->fonctionRepository = $fonctionRepo;
    }

    /**
     * Display a listing of the Fonction.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $fonctions = $this->fonctionRepository->all();

        return view('fonctions.index')
            ->with('fonctions', $fonctions);
    }

    /**
     * Show the form for creating a new Fonction.
     *
     * @return Response
     */
    public function create()
    {
        $type_fonctions = Type_fonction::All();
        return view('fonctions.create',compact('type_fonctions'));
    }

    /**
     * Store a newly created Fonction in storage.
     *
     * @param CreateFonctionRequest $request
     *
     * @return Response
     */
    public function store(CreateFonctionRequest $request)
    {
        $input = $request->all();

        $fonction = $this->fonctionRepository->create($input);

        Flash::success('Fonction saved successfully.');

        return redirect(route('fonctions.index'));
    }

    /**
     * Display the specified Fonction.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $fonction = $this->fonctionRepository->find($id);

        if (empty($fonction)) {
            Flash::error('Fonction not found');

            return redirect(route('fonctions.index'));
        }

        return view('fonctions.show')->with('fonction', $fonction);
    }

    /**
     * Show the form for editing the specified Fonction.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $fonction = $this->fonctionRepository->find($id);

        if (empty($fonction)) {
            Flash::error('Fonction not found');

            return redirect(route('fonctions.index'));
        }

        return view('fonctions.edit')->with('fonction', $fonction);
    }

    /**
     * Update the specified Fonction in storage.
     *
     * @param int $id
     * @param UpdateFonctionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFonctionRequest $request)
    {
        $fonction = $this->fonctionRepository->find($id);

        if (empty($fonction)) {
            Flash::error('Fonction not found');

            return redirect(route('fonctions.index'));
        }

        $fonction = $this->fonctionRepository->update($request->all(), $id);

        Flash::success('Fonction updated successfully.');

        return redirect(route('fonctions.index'));
    }

    /**
     * Remove the specified Fonction from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $fonction = $this->fonctionRepository->find($id);

        if (empty($fonction)) {
            Flash::error('Fonction not found');

            return redirect(route('fonctions.index'));
        }

        $this->fonctionRepository->delete($id);

        Flash::success('Fonction deleted successfully.');

        return redirect(route('fonctions.index'));
    }
}
