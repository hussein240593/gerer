<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateType_sanctionRequest;
use App\Http\Requests\UpdateType_sanctionRequest;
use App\Repositories\Type_sanctionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class Type_sanctionController extends AppBaseController
{
    /** @var  Type_sanctionRepository */
    private $typeSanctionRepository;

    public function __construct(Type_sanctionRepository $typeSanctionRepo)
    {
        $this->typeSanctionRepository = $typeSanctionRepo;
    }

    /**
     * Display a listing of the Type_sanction.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $typeSanctions = $this->typeSanctionRepository->all();

        return view('type_sanctions.index')
            ->with('typeSanctions', $typeSanctions);
    }

    /**
     * Show the form for creating a new Type_sanction.
     *
     * @return Response
     */
    public function create()
    {
        return view('type_sanctions.create');
    }

    /**
     * Store a newly created Type_sanction in storage.
     *
     * @param CreateType_sanctionRequest $request
     *
     * @return Response
     */
    public function store(CreateType_sanctionRequest $request)
    {
        $input = $request->all();

        $typeSanction = $this->typeSanctionRepository->create($input);

        Flash::success('Type Sanction saved successfully.');

        return redirect(route('typeSanctions.index'));
    }

    /**
     * Display the specified Type_sanction.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $typeSanction = $this->typeSanctionRepository->find($id);

        if (empty($typeSanction)) {
            Flash::error('Type Sanction not found');

            return redirect(route('typeSanctions.index'));
        }

        return view('type_sanctions.show')->with('typeSanction', $typeSanction);
    }

    /**
     * Show the form for editing the specified Type_sanction.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $typeSanction = $this->typeSanctionRepository->find($id);

        if (empty($typeSanction)) {
            Flash::error('Type Sanction not found');

            return redirect(route('typeSanctions.index'));
        }

        return view('type_sanctions.edit')->with('typeSanction', $typeSanction);
    }

    /**
     * Update the specified Type_sanction in storage.
     *
     * @param int $id
     * @param UpdateType_sanctionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateType_sanctionRequest $request)
    {
        $typeSanction = $this->typeSanctionRepository->find($id);

        if (empty($typeSanction)) {
            Flash::error('Type Sanction not found');

            return redirect(route('typeSanctions.index'));
        }

        $typeSanction = $this->typeSanctionRepository->update($request->all(), $id);

        Flash::success('Type Sanction updated successfully.');

        return redirect(route('typeSanctions.index'));
    }

    /**
     * Remove the specified Type_sanction from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $typeSanction = $this->typeSanctionRepository->find($id);

        if (empty($typeSanction)) {
            Flash::error('Type Sanction not found');

            return redirect(route('typeSanctions.index'));
        }

        $this->typeSanctionRepository->delete($id);

        Flash::success('Type Sanction deleted successfully.');

        return redirect(route('typeSanctions.index'));
    }
}
