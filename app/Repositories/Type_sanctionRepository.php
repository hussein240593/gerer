<?php

namespace App\Repositories;

use App\Models\Type_sanction;
use App\Repositories\BaseRepository;

/**
 * Class Type_sanctionRepository
 * @package App\Repositories
 * @version August 4, 2021, 11:07 pm UTC
*/

class Type_sanctionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'libelle'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Type_sanction::class;
    }
}
