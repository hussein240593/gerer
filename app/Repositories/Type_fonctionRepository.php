<?php

namespace App\Repositories;

use App\Models\Type_fonction;
use App\Repositories\BaseRepository;

/**
 * Class Type_fonctionRepository
 * @package App\Repositories
 * @version August 5, 2021, 4:24 pm UTC
*/

class Type_fonctionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'libelle'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Type_fonction::class;
    }
}
