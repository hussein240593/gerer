<?php

namespace App\Repositories;

use App\Models\Type_formation;
use App\Repositories\BaseRepository;

/**
 * Class Type_formationRepository
 * @package App\Repositories
 * @version August 5, 2021, 4:29 pm UTC
*/

class Type_formationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'libelle'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Type_formation::class;
    }
}
