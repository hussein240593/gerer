<?php

namespace App\Repositories;

use App\Models\Fonction;
use App\Repositories\BaseRepository;

/**
 * Class FonctionRepository
 * @package App\Repositories
 * @version August 5, 2021, 4:23 pm UTC
*/

class FonctionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_type_fonction',
        'libelle',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Fonction::class;
    }
}
