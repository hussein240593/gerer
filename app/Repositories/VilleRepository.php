<?php

namespace App\Repositories;

use App\Models\Ville;
use App\Repositories\BaseRepository;

/**
 * Class VilleRepository
 * @package App\Repositories
 * @version August 5, 2021, 5:34 pm UTC
*/

class VilleRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'libelle'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Ville::class;
    }
}
