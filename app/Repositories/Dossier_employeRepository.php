<?php

namespace App\Repositories;

use App\Models\Dossier_employe;
use App\Repositories\BaseRepository;

/**
 * Class Dossier_employeRepository
 * @package App\Repositories
 * @version August 4, 2021, 11:35 pm UTC
*/

class Dossier_employeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_employe',
        'cv',
        'lettre_motivation',
        'type_piece',
        'number_of_piece',
        'diplome'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Dossier_employe::class;
    }
}
