<?php

namespace App\Repositories;

use App\Models\Departement;
use App\Repositories\BaseRepository;

/**
 * Class DepartementRepository
 * @package App\Repositories
 * @version August 5, 2021, 4:20 pm UTC
*/

class DepartementRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nom',
        'adr_depart',
        'ville_depart'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Departement::class;
    }
}
