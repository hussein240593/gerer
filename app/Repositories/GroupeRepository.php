<?php

namespace App\Repositories;

use App\Models\Groupe;
use App\Repositories\BaseRepository;

/**
 * Class GroupeRepository
 * @package App\Repositories
 * @version August 4, 2021, 11:14 pm UTC
*/

class GroupeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Groupe::class;
    }
}
