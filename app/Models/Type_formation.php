<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Type_formation
 * @package App\Models
 * @version August 5, 2021, 4:29 pm UTC
 *
 * @property string $libelle
 */
class Type_formation extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'type_formations';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'libelle'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'libelle' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
