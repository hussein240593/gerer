<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Departement
 * @package App\Models
 * @version August 5, 2021, 4:20 pm UTC
 *
 * @property string $nom
 * @property string $adr_depart
 * @property string $ville_depart
 */
class Departement extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'departements';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'nom',
        'adr_depart',
        'tel',
        'ville_depart'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nom' => 'string',
        'adr_depart' => 'string',
        'tel' => 'string',
        'ville_depart' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
