<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employe extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected static function boot(){
        parent::boot();
        self::creating(function($model){
            // $model->slug = str_slug($model->name);
        });
    }
    protected $dates = ['deleted_at'];
    protected $guarded = [];
    protected $fillable = [
        'departement_id',
        'fonction_id',
        'nom',
        'prenom',
        'sexe',
        'email',
        'ville_id',
        'telephone',
        'type_fonction_id',
        'birthday',
        'adr_postal',
        // demission
        'date_demission',
        'motif',

    ];

       /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'foncton_id'=>'integer',
        'ville_id'=>'integer',
        'departement_id'=>'integer',
        'nom' => 'string',
        'prenom' => 'string',
        'ad_postal' => 'string',
        'sexe' => 'string',
        'email' => 'string',
        'telephone' => 'string',
        'birthday' => 'string',
        'type_fonction_id' => 'string',
        // DEMISSION
        'motif' => 'string',
        'date_demission' => 'string'
        // Dossier personne
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        // 'departement_id' => 'numeric',
        // 'nom' => 'required',
        // 'prenom' => 'required',
        // 'cv' => 'required|image|mimes:jpeg,png,jpg,gif,svg,pdf|max:2048',
        // 'lettre_motivation' => 'required|image|mimes:jpeg,png,jpg,gif,svg,pdf|max:2048',
        // 'number' => 'required',
        // 'type_piece' => 'required',
        // 'numero_piece' => 'required'
        // 'sexe' => 'required',
        // 'email' => 'required',
        // 'ville' => 'required',
        // 'telephone' => 'required',
        // 'type_fonction' => 'required',
        // 'fonction' => 'required'
    ];


    // Une departement appartient a un employe
    public function departement(){
        return $this->belongsTo(Departement::class);
    }

    public function formations(){
        return $this->belongsTo(Formation::class);
    }
    // Un employé a plusieurs absences'foreign_key', 'local_key'
    public function absence(){
        return $this->hasMany(Absence::class);
    }


    // Une fonction appartient a un employe
    public function fonction(){
        return $this->belongsTo(Fonction::class);
    }
    // Un type fonction appartient a un employe
    public function type_fonction(){
        return $this->belongsTo(Type_fonction::class);
    }

    // Une ville appartient a un employe
    public function ville(){
        return $this->belongsTo(Ville::class);
    }

        // Un employé a plusieurs absences
    // public function absences(){
    //     return $this->hasMany(Absence::class);
    // }

    // Un employé a un ou plusieurs dossier
    public function dossier_employe(){
        return $this->hasMany(Dossier_employe::class);
    }

    public function conge(){
        return $this->hasMany(Conge::class);
    }
}


