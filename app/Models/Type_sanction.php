<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Type_sanction
 * @package App\Models
 * @version August 4, 2021, 11:07 pm UTC
 *
 * @property string $libelle
 */
class Type_sanction extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'type_sanctions';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'libelle'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'libelle' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'libelle' => 'required'
    ];


    /**
         * Get all of the comments for the Type_sanction
         *
         * @return \Illuminate\Database\Eloquent\Relations\HasMany
         */
        // Une sanction a un ou plusieurs type de sanction
        public function sanction()
        {
            return $this->hasMany(Sanction::class);
        }



}
