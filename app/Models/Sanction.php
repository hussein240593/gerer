<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sanction extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $fillable = [
        'employe_id',
        'date_debut',
        'date_fin',
        'Motif',
        'type_sanction_id',
        'decisionSanction',
        'Date_de_comise_la_faute'


    ];

       /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'employe_id' => 'integer',
        'date_debut' => 'string',
        'date_fin' => 'date',
        'Motif' => 'string',
        'decisionSanction' => 'string',
        'type_sanction_id' => 'integer',
        'Date_de_comise_la_faute' => 'string'


    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

        // 'libelle' => 'required',
        // 'description' => 'required',

    ];
// Un type de sanction appartient a une sanction
    public function type_sanction()
    {
        return $this->belongsTo(Type_sanction::class);
    }
}
