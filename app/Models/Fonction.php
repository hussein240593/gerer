<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Fonction
 * @package App\Models
 * @version August 5, 2021, 4:23 pm UTC
 *
 * @property integer $id_type_fonction
 * @property string $libelle
 * @property string $description
 */
class Fonction extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'fonctions';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'id_type_fonction',
        'libelle',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_type_fonction' => 'integer',
        'libelle' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

      // Un employe a un ou plusieurs fonction
      public function employe()
      {
          return $this->hasMany(Employe::class);
      }


}
