<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Formation extends Model
{
    use HasFactory;


    protected $guarded = [];
    protected $fillable = [
        'libelle',
        'employe_id',
        'type_formation_id',
        'date_debut',
        'date_fin',
        'number_of_day'

    ];

       /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'libelle' => 'string',
        'date_debut' => 'string',
        'date_fin' => 'date',
        'employe_id' => 'integer',
        'type_formation_id' => 'integer',
        'number_of_day' => 'string'

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

        // 'libelle' => 'required',
        // 'description' => 'required',

    ];



    // public function personnes(){
    //     return $this->hasMany(Personne::class);
    // }

    // public function types_formations(){
    //     return $this->hasMany(Type_formation::class);
    // }
}
