<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Type_fonction
 * @package App\Models
 * @version August 5, 2021, 4:24 pm UTC
 *
 * @property string $libelle
 */
class Type_fonction extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'type_fonctions';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'libelle'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'libelle' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'libelle' => ' required'
    ];

    // Un employe a plusieurs type fonction
    public function employe(){
        return $this->hasMany(Employe::class);
    }


}
