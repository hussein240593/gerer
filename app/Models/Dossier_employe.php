<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Dossier_employe
 * @package App\Models
 * @version August 4, 2021, 11:35 pm UTC
 *
 * @property integer $id_employe
 * @property string $cv
 * @property string $lettre_motivation
 * @property string $type_piece
 * @property string $number_of_piece
 * @property string $diplome
 */
class Dossier_employe extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'dossier_employes';

    protected $guarded = [];
    protected $dates = ['deleted_at'];



    public $fillable = [
        'employe_id',
        'cv',
        'lettre_motivation',
        'type_piece',
        'number_of_piece',
        'avatar',
        'diplome'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'employe_id' => 'integer',
        'cv' => 'string',
        'lettre_motivation' => 'string',
        'type_piece' => 'string',
        'number_of_piece' => 'string',
        'avatar' => 'string',
        'diplome' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'employe_id' => 'required',
        'cv' => 'required',
        'type_piece' => 'required',
        // 'avatar' => 'required',
        'diplome' => 'required'
    ];

     // Un dossier appartient a un employe
    public function employe(){
        return $this->belongsTo(Employe::class);
    }


}
