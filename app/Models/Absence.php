<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Absence extends Model
{
    use HasFactory;
    protected $dates = ['deleted_at'];
    protected $guarded = [];
    protected $fillable = [
        'employe_id',
        'date_absence',

        // Justification d'absence
        'rapport_explication',
        // status
        'is_status_present',
        // Accepte ou refus demission
        'is_etat_accepte_demission'
        // 'date_fin'

    ];

       /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'employe_id' => 'integer',
        'date_absence' => 'string',
         // Justification d'absence
        'rapport_explication' => 'string',
        // Status pour le pointage,demision,absence et presence
        'is_status_present' => 'string',
        // Accepter ou refus de la demission
        'is_etat_accepte_demission' => 'string'
        // 'date_fin' => 'string'

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

        'is_status_present' => 'required',
        // 'description' => 'required',

    ];

    // public function fonctions(){
    //     return $this->hasMany(Fonction::class);
    // }
    public function employe(){
        return $this->belongsTo(Employe::class);
    }
}
