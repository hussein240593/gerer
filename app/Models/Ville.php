<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Ville
 * @package App\Models
 * @version August 5, 2021, 5:34 pm UTC
 *
 * @property string $libelle
 */
class Ville extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'villes';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'libelle'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'libelle' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'libelle' => 'required'
    ];

    // employe est un ou plusisurs ville
    public function ville(){
        return $this->hasMany(Ville::class);
    }


}
