<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Conge extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $fillable = [
        'employe_id',
        'date_debut',
        'date_fin',
        'motif_conge',
        'lettre_autorisation',
        'nombre_jours_conge'

    ];

       /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'employe_id' => 'integer',
        'date_debut' => 'string',
        'date_fin' => 'date',
        'motif_conge' => 'string',
        'lettre_autorisation' => 'string',
        'nombre_jours_conge' => 'string',

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

        // 'libelle' => 'required',
        // 'motif_conge' => 'required|min:10',

    ];

    // Un conge appartient a un employe
    public function employe(){
        return $this->belongsTo(Employe::class);
    }
}
