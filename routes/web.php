<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::group(['middleware'=>'auth'], function(){

    // TYPE DE FORMATION
    Route::resource('typeFormations','TypeFormationController');

    // TYPE FONCTION
    Route::resource('typeFonctions','TypeFonctionController');


    // DEPARTEMENT
    Route::resource('departements','DepartementController');
    // FONCTION
    Route::resource('fonctions','FonctionController');
    Route::get('fonctions/{slug}','FonctionController@show')->name('fonctions.show');
});

// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');



Route::resource('employes','EmployeController');
// Route::get('employes/{slug}','EmployeController@show')->name('employes.show');
// Route::get('employes/{slug}','EmployeController@edit')->name('employes.edit');
Route::get('demission-employes/{id}','EmployeController@Demission')->name('demission');

// route training
Route::resource('formations','FormationController');

// gestion des sanction
Route::resource('sanctions','SanctionController');
Route::post('sanctions/{id}','SanctionController@decision')->name('decision.index');






// ABSENCES
Route::resource('absences','AbsenceController');

Route::post('absences/{id}','AbsenceController@justify')->name('justify.index');
// Route::post('absences-arpport/{id}','AbsenceController@rapport')->name('rapport.index');
Route::get('/absences-demission-accepte/{id}', 'AbsenceController@Etat_accepte_demission')->name('accepte_demission');
Route::get('/absences-demission-refuse/{id}','AbsenceController@Etat_refus_demission')->name('refuse_demission');


// CONGES
Route::resource('conges','CongeController');
Route::delete('conges/{slug}', 'CongeController@destroy')->name('conges.destroy');

// Route::get('/liste-conges-en-cours', 'CongeController@listeCongesEnAttente')->name('CongeEnAttente');

Route::get('/conge-valide/{id}', 'CongeController@Accepte')->name('congeValide');
Route::get('/rejetter/{id}','CongeController@Refuse')->name('congeRefuse');
Route::post('/conges/{id}','CongeController@Autorisation')->name('autorisations');



Route::resource('groupes', 'GroupeController');


Route::resource('typeSanctions', 'Type_sanctionController');


Route::resource('dossierEmployes', 'Dossier_employeController');
Route::post('dossier', 'EmployeController@Dossier')->name('dossier.store');


Route::resource('typeFonctions', 'Type_fonctionController');


Route::resource('typeFormations', 'Type_formationController');
Route::get('send', 'HomeController@sendNotification');


Route::resource('villes', 'VilleController');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
